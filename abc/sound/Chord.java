package abc.sound;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//Immutable datatype representing a chord or music.
public class Chord implements Note{

    //Abstraction Function;
    //      Represents a chord as a list of notes to be played at the same time with length of length.
    //Rep Invariant:
    //      Notes.size > 0, length of chord = length of first note in notes list, all notes in list are "chordable"
    //Safety from Rep Exposure:
    //      All fields are private, all public methods return primatives or new objects
    
    private final List<Note> notes;
    private double length;
    
    /**
     * Creator method for Chord where a new note is created that plays all notes in the
     * inputNotes list simultaneously.
     * @param inputNotes, the list of notes in the chord.
     */
    public Chord(List<Note> inputNotes){
        notes = new ArrayList<Note>(inputNotes);
        length = notes.get(0).length();
        checkRep();
    }
    
    private void checkRep(){
        assert(notes.size() > 0);
        for(Note note : notes){
            assert(note.chordable());
        }
        //not sure if I need this line.
        assert(length == notes.get(0).length());
    }
    
    @Override
    public int hashCode(){
        int code = 0;
        for (Note note: notes){
            code = code + note.hashCode();
        }
    return code;
    }
    
    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        builder.append("Chord:" + '\n');
        for( Note note : notes){
            builder.append(note.toString() + '\n');
        }
        return builder.toString();
    }

    @Override
    public Note applyMeasure(Map<String, Integer> inputSemitones, Map<String, Integer> key) {
        List<Note> newList = new ArrayList<>();
        for (Note note : notes){
            newList.add(note.applyMeasure(inputSemitones, key));
        }
        return new Chord(newList);
    }

    @Override
    public int addTo(SequencePlayer player, int startIndex, double scale) {
        int end = startIndex;
        for (int noteNum = 0; noteNum < notes.size(); noteNum++){
            if (noteNum == 0){
            end = notes.get(noteNum).addTo(player, startIndex, scale);}
            else notes.get(noteNum).addTo(player, startIndex, scale);
        }
        return end;
    }
    
    @Override
    public boolean tupletable(){
        return true;
    }
    
    @Override
    public boolean chordable(){
        return false;
    }
    
    @Override
    public double length(){
        return length;
    }
    
    @Override
    public boolean equals(Object object) {
        if ( !(object instanceof Chord) ) return false;
        Chord that = (Chord) object;
        return this.notes.equals(that.notes) && this.length == that.length;
    }
}
