package abc.sound;

import java.util.Map;

//Immutable datatype representing a tuplet of two music notes.
public class Duplet implements Note {

    //Abstraction function:
    //      Represents a tuplet of two notes as two 'tupletable' notes with length and scale of 1.5X the original length.
    //Rep Invariant:
    //      Both notes are 'tupletable', length > 0;
    //Safety from Rep Exposure:
    //      All fields are private
    //      All methods return primative types or new objects
    
    
    private final Note note1;
    private final Note note2;
    private final double length;
    private static final double SCALE = 1.5;
    
    /**
     * Constructor method for Duplet, which makes a duplet out two 'tupletable' notes.
     * @param inputNote1 
     * @param inputNote2
     */
    public Duplet(Note inputNote1, Note inputNote2){
        note1 = inputNote1;
        note2 = inputNote2;
        length = (note1.length() + note2.length())*SCALE;
        checkRep();
    }
    
    private void checkRep(){
        assert(note1.tupletable());
        assert(note2.tupletable());
        assert(length > 0);
    }
    
    @Override
    public int hashCode(){
        return note1.hashCode() + note2.hashCode();
    }
    
    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        builder.append("Duplet:" + '\n');
        builder.append(note1.toString() + '\n');
        builder.append(note2.toString() + '\n');
        return builder.toString();
    }

    @Override
    public Note applyMeasure(Map<String, Integer> inputSemitones, Map<String, Integer> key) {
        return new Duplet(note1.applyMeasure(inputSemitones, key), note2.applyMeasure(inputSemitones, key));
    }

    @Override
    public int addTo(SequencePlayer player, int startIndex, double scale) {
        int middle = note1.addTo(player, startIndex, SCALE);
        int end = note2.addTo(player, middle, SCALE);
        return end;
    }
    
    @Override
    public double length(){
        return length;
    }
    
    @Override
    public boolean tupletable(){
        return false;
    }
    
    @Override
    public boolean chordable(){
        return false;
    }

    @Override
    public boolean equals(Object object) {
        if ( !(object instanceof Duplet) ) return false;
        Duplet that = (Duplet) object;
        return this.note1.equals(that.note1) && this.length == that.length && this.note2.equals(that.note2); 
    }
}
