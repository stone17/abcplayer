package abc.sound;

import java.util.List;
import java.util.Map;

/**
 * A generic data structure that represents the various structures in a music piece.
 * These are measures, and the markers for repeated sections.
 *
 */
public interface Structure {
    
    /**
     * Create a structure that represents the beginning of a repeated
     *  section which returns true for beginRepeat(). 
     * @return
     */

    static Structure makeBeginRepeat() {
       return new RepeatMarker(true, false, false);
    }
    
    /**
     * Create a structure that represents the end of a repeated
     *  section which returns true for endRepeat(). 
     * @return
     */
    static Structure makeEndRepeat() {
        return new RepeatMarker(false, true, false);
    }
    
    /**
     * Create a structure that represents a first ending, 
     *  which returns true for firstEnding().
     * @return the structure as described.
     */
    static Structure makeFirstEnding() {
        return new RepeatMarker(false, false, true);
    }
    
    /**
     * @return a list of notes in the structure, if the structure hasNotes. Otherwise
     *   throws an Unsupported Operation exception.
     */
    public List<Note> getNotes();

    /**
     * @return true iff the structure contains notes
     */
    public boolean hasNotes();
    
    /**
     * @return true iff the structure represents the beginning of a repeated section
     */
    public boolean beginRepeat();
    
    /**
     * @return true iff the structure represents the end of a repeated section
     */
    public boolean endRepeat();
    
    /**
     * @return true iff the structure represents the beginning of a first ending
     */
    public boolean firstEnding();
}
