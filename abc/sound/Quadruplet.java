package abc.sound;

import java.util.Map;

//An immutable datatype the implements note representing 4 notes played in the time of 3.
public class Quadruplet implements Note{
    
    //Abstraction function:
    //      Represents a quadruplet of four notes as two 'tupletable' notes with length and scale of 0.75x the original length.
    //Rep Invariant:
    //      Both notes are 'tupletable', length > 0;
    //Safety from Rep Exposure:
    //      All fields are private
    //      All methods return primative types or new objects

    private final Note note1;
    private final Note note2;
    private final Note note3;
    private final Note note4;
    private final double length;
    private static final double SCALE = .75;
    
    /**
     * Constructor method for Quadruplet that takes 4 'tupletable' notes.
     * @param inputNote1
     * @param inputNote2
     * @param inputNote3
     * @param inputNote4
     */
    public Quadruplet(Note inputNote1, Note inputNote2, Note inputNote3, Note inputNote4){
        note1 = inputNote1;
        note2 = inputNote2;
        note3 = inputNote3;
        note4 = inputNote4;
        length = (note1.length() + note2.length() + note3.length() + note4.length())*SCALE;
        checkRep();
        //might need check to make sure notes aren't tuples.
        //not sure if we can have tuples within tuples.
        //if notes within tuples can't be rests, I'll check for that too.
    }
    
    private void checkRep(){
        assert(note1.tupletable());
        assert(note2.tupletable());
        assert(note3.tupletable());
        assert(note4.tupletable());
        assert(length > 0);
    }
    
    @Override
    public int hashCode(){
        return note1.hashCode() + note2.hashCode() + note3.hashCode() + note4.hashCode();
    }
    
    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        builder.append("Quadruplet:" + '\n');
        builder.append(note1.toString() + '\n');
        builder.append(note2.toString() + '\n');
        builder.append(note3.toString() + '\n');
        builder.append(note4.toString() + '\n');
        return builder.toString();
    }

    @Override
    public Note applyMeasure(Map<String, Integer> inputSemitones, Map<String, Integer> key) {
        return new Quadruplet(note1.applyMeasure(inputSemitones, key), note2.applyMeasure(inputSemitones, key),
                note3.applyMeasure(inputSemitones, key),note4.applyMeasure(inputSemitones, key));
    }

    @Override
    public int addTo(SequencePlayer player, int startIndex, double scale) {
        int middle1 = note1.addTo(player, startIndex, SCALE);
        int middle2 = note1.addTo(player, middle1, SCALE);
        int middle3 = note1.addTo(player, middle2, SCALE);
        int end = note2.addTo(player, middle3, SCALE);
        return end;
    }
    
    @Override
    public boolean tupletable(){
        return false;
    }
    
    @Override
    public boolean chordable(){
        return false;
    }
    
    @Override
    public double length(){
        return length;
    }

    @Override
    public boolean equals(Object object) {
        if ( !(object instanceof Quadruplet) ) return false;
        Quadruplet that = (Quadruplet) object;
        return this.note1.equals(that.note1) && this.length == that.length && this.note2.equals(that.note2)
                && this.note3.equals(that.note3) && this.note4.equals(that.note4); 
    }
}
