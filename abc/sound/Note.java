package abc.sound;

import java.util.Map;

// An immutable data-type representing a music note, implemented by:
//      - Single: a single note
//      - Duplet: two notes played with a length of three
//      - Triple: three notes played with a length of two
//      - Quadruplet: four notes played with a length of three
//      - Chord: multiple notes played at the same time
public interface Note {
    
    /**
     * String Representation of a Note
     * @return String representing the Note
     */
    public String toString();
    
    /**
     * Applies accidental to the note dependent on the semi-tones already used in the measure.
     * If the note that this is called on has a semi-tone mapping for its base, then inputSemitones
     * is updated to reflect that. Otherwise, if the inputSemitones contains a mapping for the base of
     * the note this is called on, then the a new Note with that semitone applied is returned.
     * The same is true for the Key of the piece, but accidentals seen proir in the measure take precedent
     * over accidentials that should be applied due to the key signature.
     * @param inputSemitones: a map of base values and the semitones that should be applied.
     * @param key: a map of base values and the semitones that should be applied.
     * @return a new Note with the correct semitones applied
     */
    public Note applyMeasure(Map<String, Integer> inputSemitones, Map<String, Integer> key);
    
    /**
     * Adds the note this is called on the specified Sequence player.
     * @param player: the SequencePlayer the note will be added to
     * @param startIndex: the beginning tick to add the note to
     * @param scale: how long of the original value the note should be added for
     * @return
     */
    public int addTo(SequencePlayer player, int startIndex, double scale);
    
    /**
     * @return the length of the notes in beats
     */
    public double length();
    
    /**
     * @return true iff the note can be in a tuple.
     * This is true for singles, chords, and rests
     */
    public boolean tupletable();
    
    /**
     * @return true iff the note can be in a chord.
     * this is true only for singles.
     */
    public boolean chordable();

}
