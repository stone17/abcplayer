package abc.sound;

import java.util.Map;

//Immutable datatype representing 3 musical notes played in the time of 2.
public class Triplet implements Note {

    //Abstraction function:
    //      Represents a triplet of three notes as three 'tupletable' notes with length and scale of 2/3 the original length.
    //Rep Invariant:
    //      All notes are 'tupletable', length > 0;
    //Safety from Rep Exposure:
    //      All fields are private
    //      All methods return primative types or new objects
    
    private final Note note1;
    private final Note note2;
    private final Note note3;
    private final double length;
    private static final double SCALE = 2.0/3.0;
    
    /**
     * Constructor method for triplet that takes three 'tupletable' notes.
     * @param inputNote1
     * @param inputNote2
     * @param inputNote3
     */
    public Triplet(Note inputNote1, Note inputNote2, Note inputNote3){
        note1 = inputNote1;
        note2 = inputNote2;
        note3 = inputNote3;
        length = (note1.length() + note2.length() + note3.length())*SCALE;
        checkRep();
    }
    
    private void checkRep(){
        assert(note1.tupletable());
        assert(note2.tupletable());
        assert(note2.tupletable());
        //not sure if checkRep needs these last 3 lines, since that's literally how its defined.
        //also, its an immutable value.
        //assert(note1.length() + note2.length() + note3.length() == length*1.5);
    }
    
    @Override
    public int hashCode(){
        return note1.hashCode() + note2.hashCode() + note3.hashCode();
    }
    
    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        builder.append("Triplet:" + '\n');
        builder.append(note1.toString() + '\n');
        builder.append(note2.toString() + '\n');
        builder.append(note3.toString() + '\n');
        return builder.toString();
    }

    @Override
    public Note applyMeasure(Map<String, Integer> inputSemitones, Map<String, Integer> key) {
        return new Triplet(note1.applyMeasure(inputSemitones, key), note2.applyMeasure(inputSemitones, key),
                note3.applyMeasure(inputSemitones, key));
    }

    @Override
    public int addTo(SequencePlayer player, int startIndex, double scale) {
        int middle1 = note1.addTo(player, startIndex, SCALE);
        int middle2 = note2.addTo(player, middle1, SCALE);
        int end = note3.addTo(player, middle2, SCALE);
        return end;
    }
    
    @Override
    public double length(){
        return length;
    }
    
    @Override
    public boolean tupletable(){
        return false;
    }
    
    @Override
    public boolean chordable(){
        return false;
    }

    @Override
    public boolean equals(Object object) {
        if ( !(object instanceof Triplet) ) return false;
        Triplet that = (Triplet) object;
        return this.note1.equals(that.note1) && this.length == that.length 
                && this.note3.equals(that.note3); 
    }
}
