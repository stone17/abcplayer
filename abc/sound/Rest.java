package abc.sound;

//An immutable datatype representing a pause in a piece of music.
import java.util.Map;

public class Rest implements Note {

    //Abstraction function:
    //      Represents a rest with a specified length
    //Rep Invariant:
    //      length > 0;
    //Safety from Rep Exposure:
    //      All fields are private
    //      All methods return primative types or new objects
    
    private double length;
    
    public Rest(String rest){
        String frac = rest.substring(1);
        if(frac.length() == 0) length = 1;
        else if(frac.indexOf('/') == 0){
            if(frac.length() == 1){
                length = 0.5;
            }
            else{
                length = Integer.parseInt(frac.substring(1));                
            }
        }
        else if(frac.indexOf('/') < 0){
            length = Integer.parseInt(frac);
        }
        else{
            String[] numbs = frac.split("/"); //removes the z, and then splits the fraction.
            length = (double) Integer.parseInt(numbs[0]) / Integer.parseInt(numbs[1]);
        }
    }
    
    @Override
    public int hashCode(){
        return (int) length;
    }

    @Override
    public Note applyMeasure(Map<String, Integer> inputSemitones, Map<String, Integer> key) {
        return this;
    }

    @Override
    public int addTo(SequencePlayer player, int startIndex, double scale) {
        // TODO Auto-generated method stub
        return startIndex + (int) Math.round(length*Song.ticksPerBeat*scale);
    }

    @Override
    public double length() {
        // TODO Auto-generated method stub
        return length;
    }

    @Override
    public boolean tupletable() {
        return true;
    }

    @Override
    public boolean chordable() {
        return false;
    }
    
    public String toString(){
        return "restlength: " + length;
    }

    @Override
    public boolean equals(Object object) {
        if ( !(object instanceof Rest) ) return false;
        Rest that = (Rest) object;
        return this.length == that.length;
    }
    
}
