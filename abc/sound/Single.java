package abc.sound;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//An immutable data-type that implements Note.
public class Single implements Note{

    private final Map<String, Integer> semitones;
    private final String base;
    private double length;
    private final Pitch pitch;
    private static final String valids = "ABCDEFGabcdefg";
    
    //A constructor to build a new Single based on a String from ANTLR.
    public Single(String input){
        semitones = new HashMap<String, Integer>();
        StringBuilder baseBuilder = new StringBuilder();
        Integer tones = 0;
        Pitch tempPitch = null;
        List<Character> charList = new ArrayList<Character>();
        //Create character list from input String
        for (int letter = 0; letter < input.length(); letter++){
            charList.add(input.charAt(letter));
        }
        //Iterate through the list until an acceptable base is found.
        for (int letter = 0; letter < charList.size(); letter++){
            if ( (valids.indexOf(charList.get(letter)) != -1)){
                tempPitch = new Pitch(Character.toUpperCase(charList.get(letter)));
                baseBuilder.append(charList.get(letter));
                //Once found, add an octave if lower case, and remove it from the charList.
                if (Character.isLowerCase(charList.get(letter))){
                    tempPitch = tempPitch.transpose(Pitch.OCTAVE);
                }
                charList.remove(letter);
            }
        }
        //For every remaining character, look for semitone, and add them to the semitones Map,
        //and transpose the pitch accordingly.
        boolean toned = false;
        for (int letter = 0; letter < charList.size(); letter++){
            if (charList.get(letter).equals(',')){ 
                baseBuilder.append(',');
                tempPitch = tempPitch.transpose(-Pitch.OCTAVE);
                charList.remove(letter);
                letter--;
            }
            else if (charList.get(letter).equals('\'')){
                baseBuilder.append('\'');
                tempPitch = tempPitch.transpose(Pitch.OCTAVE);
                charList.remove(letter);
                letter--;
            }
            
            else if (charList.get(letter).equals('^')){
                toned = true;
                tones++;
                tempPitch = tempPitch.transpose(1);
                charList.remove(letter);
                letter--;
            }
            else if (charList.get(letter).equals('_')){
                toned = true;
                tones--;
                tempPitch = tempPitch.transpose(-1);
                charList.remove(letter);
                letter--;
            }
            else if (charList.get(letter).equals('=')){
                toned = true;
                tones = 0;
                charList.remove(letter);
                letter--;
            }
        }
        pitch = tempPitch;
        //Calculate length of note
        if (charList.contains('/')){
            int numerator = 1;
            int denominator = 2;
            if(charList.indexOf('/') != 0){numerator = Integer.parseInt(charList.get(0).toString());}
            if(charList.indexOf('/') != charList.size()-1){denominator = Integer.parseInt(charList.get(charList.indexOf('/') + 1).toString());}
            length = numerator/ (double) denominator;
            }
        else if(charList.size() == 0){length = 1;}
        else {length = Integer.parseInt(charList.get(0).toString());}
        
        base = baseBuilder.toString();
        if (toned){semitones.put(base, tones);}
        checkRep();

    }
    
    //Constructor to build a new Single based on an old one.
    public Single(Pitch inputPitch, Map<String, Integer> inputSemitones, String inputBase, double inputLength){
        semitones = new HashMap<String, Integer>(inputSemitones);
        pitch = inputPitch;
        length = inputLength;
        base = inputBase;
        checkRep();
    }

    private void checkRep(){
        assert(pitch != null);
        assert(length > 0);
        //need more of a checkRep for base.
    }
    
    @Override
    public int hashCode(){
        return (int) length;
    }
    
    @Override
    public String toString(){
        StringBuilder string = new StringBuilder();       
        string.append("Pitch: " + pitch.toString() + ", ");
        string.append("Base: " + base + ", ");
        string.append("Length: " + length + ", ");
        string.append("Semitones: " + semitones.toString() + ".");
        return string.toString();
    }
    
    @Override
    public boolean equals(Object object) {
        if ( !(object instanceof Single) ) return false;
        Single that = (Single) object;
        boolean sameSemitones;
        // this has sharps or flats
        if (this.semitones.containsKey(this.base)) {
            // that has sharps or flats
            if (that.semitones.containsKey(that.base)) {
                sameSemitones = this.semitones.get(this.base) == (that.semitones.get(that.base)); // are they the same?
            // that has no sharps or flats
            } else {
                sameSemitones = this.semitones.get(this.base).equals(0); // this needs to have a natural
            }
        // this doesn't have sharps or flats
        } else {
            // that has sharps or flats
            if (that.semitones.containsKey(that.base)) {
                sameSemitones = (that.semitones.get(that.base)).equals(0); // that needs to be natural
            // that has no sharps or flats
            } else {
                sameSemitones = true;
            }
        }
        return this.base.equals(that.base) && this.length == that.length && this.pitch.equals(that.pitch) 
                && sameSemitones;
    }

    @Override
    public Note applyMeasure(Map<String,Integer> inputSemitones, Map<String, Integer> key){
        // it's explictly given accidentals:
        if (semitones.containsKey(base)){
            inputSemitones.put(base, semitones.get(base));
            return new Single(pitch, semitones, base, length);
        }
        // the measure has accidentals:
        else if (inputSemitones.containsKey(base)){
            semitones.put(base, inputSemitones.get(base));
            return new Single(pitch.transpose(inputSemitones.get(base)), semitones, base, length);
        }
        else {
            for (String note : key.keySet()){
                // the key confers sharps and flats:
                if (base.toUpperCase().contains(note)){
                    semitones.put(base, key.get(base.toUpperCase()));
                    return new Single(pitch.transpose(key.get(note)), semitones, base, length);
                }
            }
        }
        // it doesn't have any sharps or flats:
        return new Single(pitch, semitones, base, length);
    }

    @Override
    public int addTo(SequencePlayer player, int startIndex, double scale) {
        player.addNote(pitch.toMidiNote(), startIndex, (int) Math.round(length*Song.ticksPerBeat*scale));
        return startIndex + (int) Math.round(length*Song.ticksPerBeat*scale);
    }
    
    @Override
    public boolean tupletable(){
        return true;
    }

    @Override
    public boolean chordable(){
        return true;
    }
    
    @Override
    public double length(){
        return length;
    }
    
    public int[] pitch(){
        return new int[] {pitch.toMidiNote()};
    }
}
