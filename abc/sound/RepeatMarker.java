package abc.sound;

import java.util.List;
import java.util.Map;

/** An immutable type that represents the markers of repeated sections in music (i.e., repeats and endings) */
class RepeatMarker implements Structure {
    private final boolean beginRepeat;
    private final boolean endRepeat;
    private final boolean ending;
    
    // AF: represents a marker for a repeated section: the beginning of a repeated section if beginRepeat, 
    //       the end of a repeated section if endRepeat, or the beginning of a first ending if ending
    // RI: fields should be mutually exclusive.
    // RE: all fields are final and private.
    
    public RepeatMarker(boolean beginRepeat, boolean endRepeat, boolean ending) {
        this.beginRepeat = beginRepeat;
        this.endRepeat = endRepeat;
        this.ending = ending;
        checkRep();
    }
    
    private void checkRep() {
        assert beginRepeat ^ endRepeat ^ ending;
    }
    
    @Override
    public List<Note> getNotes() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean hasNotes() {
        return false;
    }

    @Override
    public boolean beginRepeat() {
        return beginRepeat;
    }

    @Override
    public boolean endRepeat() {
        return endRepeat;
    }

    @Override
    public boolean firstEnding() {
        return ending;
    }

    @Override
    public int hashCode() {
        return Boolean.hashCode(beginRepeat) + Boolean.hashCode(endRepeat) + Boolean.hashCode(ending);
    }
    
    @Override
    public boolean equals(Object object) {
        if ( !(object instanceof RepeatMarker) ) return false;
        RepeatMarker that = (RepeatMarker) object;
        return beginRepeat() == that.beginRepeat() && endRepeat() == that.endRepeat
                && firstEnding() == that.firstEnding();
    }
    
    @Override
    public String toString() {
        if (beginRepeat) {
            return "Begin Repeat";
        } else if (endRepeat) {
            return "End Repeat";
        } else { // it's a first ending
            return "First Ending";
        }
    }
}
