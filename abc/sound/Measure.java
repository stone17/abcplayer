package abc.sound;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// An immutable type representing a measure of a song in Abc notation.
public class Measure implements Structure {

    private final List<Note> notes = new ArrayList<>();
    private final int num;
    private final int den;
    private final double length;
    
    // AF: represents a measure of music. notes represents the notes in the measure, in order. Has time signature num / den.
    //      Has length beats.
    
    /**
     * Create a new measure. The key determines the accidentals for the measure, if they are not explicitly
     * overridden by accidentals on the notes themselves. Has time signature num / den.
     * @param inputNotes a list of notes to be in the measure
     * @param key a mapping from the capitalized base to a pos/neg 1 if the base has a sharp/flat.
     * @param num the number of beats per measure
     * @param den the note that gets the beat (4 for a quarter, 8 for an eigth, etc.)
     */
    public Measure(List<Note> inputNotes, Map<String, Integer> key, int num, int den){
        Map<String,Integer> semitones = new HashMap<String, Integer>();
        double len = 0;
        for (Note note : inputNotes){
            notes.add(note.applyMeasure(semitones, key));
            len += note.length();
        }
        length = len;
        this.num = num;
        this.den = den;
        checkRep();
    }
    private void checkRep(){
        assert(notes != null);
        //assert((double) num / den == length);
    }
    
    /**@return a list of notes used in the measure*/
    public List<Note> getNotes(){
        return new ArrayList<Note>(notes);
    }
    
    @Override
    public int hashCode(){
        int code = 0;
        for (Note note : notes){
            code = code + note.hashCode();
        }
        return code;
    }
    
    @Override
    public boolean hasNotes() {
        return true;
    }
    
    // Doesn't support or have these properties:
    @Override public boolean beginRepeat() { return false; }
    @Override public boolean endRepeat() { return false; }
    @Override public boolean firstEnding() { return false; }
    
    @Override
    public String toString() {
        String out = "[";
        for (Note note : notes) {
            out = out + note.toString() + ", ";
        }
        out.subSequence(0, out.length()-2);
        out += "]";
        return out;
    }
    
    @Override
    public boolean equals(Object object) {
        if ( !(object instanceof Measure) ) return false;
        Measure that = (Measure) object;
        return this.notes.equals(that.notes) && this.num == that.num
                && this.den == that.den; 
    }
}
