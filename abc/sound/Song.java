package abc.sound;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;

import org.antlr.v4.gui.Trees;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import abc.parser.BodyLexer;
import abc.parser.BodyParser;
import abc.parser.HeaderLexer;
import abc.parser.HeaderParser;
import abc.parser.MakeBody;
import abc.parser.MakeHeader;


// An immutable data-type representing a song.

public class Song {

    public static final int ticksPerBeat = 12*16;
    private final int index;
    private final String title;
    private final String key;
    private final String composer;
    private final int tempo;
    private final double meterHigh;
    private final double meterLow;
    private final double lengthHigh;
    private final double lengthLow;
    private final double tempoHigh;
    private final double tempoLow;
    //key, tempo, and meter can change in the piece, so you might not want this to be final.
    //We might want to have measures have their own key and meter.
    
    //AST:  represents the entire song, 
    
    private final Map<Character,String> header;
    private final Map<String, List<Structure>> body;
    
    /**
     * Constructor Method for Song
     * @param inputHeader Map of header parameters, must contain I,T,K as keys
     * @param inputBody List of measures that represents the body of the song
     */
    public Song(Map<Character,String> inputHeader, Map<String, List<Structure>> inputBody){
        header = new HashMap<Character,String>(inputHeader);
        body = new HashMap<String, List<Structure>>(inputBody);
        
        index = Integer.valueOf(header.get('I'));
        title = header.get('T');
        key = header.get('K').split(":")[1].trim();
        
        if (header.containsKey('C')){ composer = header.get('C');}
        else composer = "Unknown";
        String highString, lowString;
        //METER
        if (header.containsKey('M')){
            String meterString = header.get('M').split(":")[1].trim();
            highString = meterString.split("/")[0];
            lowString = meterString.split("/")[1];
            meterHigh = Integer.parseInt(highString);
            meterLow = Integer.parseInt(lowString);
        }
        else {meterHigh = 4; meterLow = 4;}
        
        //DEFAULT NOTE LENGTH
        if (header.containsKey('L')){
            String lengthString = header.get('L').split(":")[1].trim();
            highString = lengthString.split("/")[0];
            lowString = lengthString.split("/")[1];
            lengthHigh = Integer.parseInt(highString);
            lengthLow = Integer.parseInt(lowString);
        }        
        else if (meterHigh / meterLow >= 0.75){lengthHigh = 1; lengthLow = 8;}
        else {lengthHigh = 1; lengthLow = 16;}        
        
        //TEMPO
        int tempTempo=0;
        if (header.containsKey('Q')){
            String tempoString = header.get('Q').split(":")[1].trim();
            String tempoFraction = tempoString.split("=")[0];
            String tempoValue = tempoString.split("=")[1];
            highString = tempoFraction.split("/")[0];
            lowString = tempoFraction.split("/")[1];
            tempoHigh = Integer.parseInt(highString);
            tempoLow = Integer.parseInt(lowString);
            tempTempo = Integer.parseInt(tempoValue);
        }
        else {tempTempo = 100; tempoHigh = lengthHigh; tempoLow = lengthLow;}
        
        //TEMPO IN TERMS OF DEFAULT LENGTH
        tempo = (int) (tempTempo * tempoHigh / tempoLow * lengthLow / lengthHigh);
    }
    

    
    /**
     * Adds a Structure to the body of a song
     * @param newStructure: Structure to be added
     * @return a new Song, identical to the first, with the added Structure to the body
     */
    public Song addStructure(String voiceName, Structure newStructure){
        List<Structure> newVoice = new ArrayList<Structure>(body.get(voiceName));
        newVoice.add(newStructure);
        body.put(voiceName, newVoice);
        return new Song(new HashMap<Character,String>(header), body);
    }
    
    /**
     * toString method for type Song
     * @return a String where the header is added to the String in accordance with the toString method of
     * Map, and each measure is on a following line according to the toString method of Measure
     */
    public String toString(){
        StringBuilder builder = new StringBuilder(header.toString());
        builder.append('\n');
        for (String voiceName: body.keySet()){
            List<Structure> voice = body.get(voiceName);
            for (Structure structure: voice){
                builder.append(structure.toString());
            }
            builder.append('\n'); 
        }
        return builder.toString();
    }
    
    /**
     * Plays all voices in a song starting at once after processing each voice.
     * Plays song with repeats added accordingly.
     */
    public void play(){
        List<SequencePlayer> voices = new ArrayList<SequencePlayer>();
        for (String voiceName : body.keySet()){
            List<Structure> voice = body.get(voiceName);    
            try{
            int startPoint = 0;
            SequencePlayer player = new SequencePlayer(tempo, ticksPerBeat);
            // expand all the repeated sections into a big list of measures:
            List<Structure> expandedBody = new ArrayList<>();
            for (int i=0; i < voice.size(); i++){
                Structure structure = voice.get(i);
                // read measures:
                if (structure.hasNotes()) {
                    expandedBody.add(structure);
                }
                // look for repeats:
                if (structure.endRepeat()) {
                    // look backwards for the accompanying beginRepeat, if it exists:
                    int endIndex = i;
                    for (int j=endIndex-1; j>=0; j--) {
                        if (voice.get(j).firstEnding()) {
                            endIndex = j;
                        } else if (voice.get(j).beginRepeat()) {
                            int startIndex = j+1; // the measure after the marker
                            expandedBody.addAll(slice(voice, startIndex, endIndex));
                            break;
                        } else if (j==0) { // if it doesn't exist, go back to the beginning of the piece
                            expandedBody.addAll(slice(voice, 0, endIndex));
                        }
                    }
                }
            }
            // add all the notes to the player:
            for (Structure structure : expandedBody) {
                if (structure.hasNotes()) {
                    for (Note note : structure.getNotes()){
                        startPoint = note.addTo(player, startPoint, 1);
                    }

                }
            }
            voices.add(player);
            } catch (MidiUnavailableException | InvalidMidiDataException e){};
        }
        try{
        for (SequencePlayer player : voices){
            player.play();
            } 
        } catch (MidiUnavailableException e){}
    }
    
    /**
     * Parses the input file into a song and then plays the song.
     * @param file to be parsed
     */
    public static void parseAndPlay(File file){
        Song song = Song.parse(file);
        song.play();
    }
    
    /**
     * Parses an input file and returns an instance of Song.
     * Parsing is conducted in accordance with Body.g4 and Header.g4
     * Throws Runtime Exception.
     * @param file
     * @return a Song representing the input file.
     */
    static Song parse(File file) {
        FileReader reader;
        Song song;
        ParseTreeWalker headerWalker;
        ParseTree headerTree;
        MakeHeader headerListener;
        HeaderParser headerParser;

        ParseTreeWalker bodyWalker;
        ParseTree bodyTree;
        MakeBody bodyListener;
        BodyParser bodyParser;
        
        Map<Character, String> header;
        Map<String, List<Structure>> body;

        try{
            reader = new FileReader(file);}
        catch(FileNotFoundException e){ throw new RuntimeException("File not found.");}
        try{
            String line;
            BufferedReader bufReader = new BufferedReader(reader);
            StringBuilder builder = new StringBuilder();
            while((line = bufReader.readLine()) != null){
                builder.append(line);
                builder.append('\n');
            }
            String fileString = builder.toString();
            List<String> pieceParts = Song.splitPiece(fileString);
            System.out.println(pieceParts.get(0));

            //PARSING THE HEADER
            CharStream headerStream = new ANTLRInputStream(pieceParts.get(0));
            HeaderLexer headerLexer = new HeaderLexer(headerStream);
            TokenStream headerTokens = new CommonTokenStream(headerLexer);
            headerParser = new HeaderParser(headerTokens);      
            headerTree = headerParser.root();      
            headerWalker = new ParseTreeWalker();
            headerListener = new MakeHeader();
            headerWalker.walk(headerListener, headerTree);
            header = headerListener.getHeader();
            String tempKey = header.get('K').split(":")[1].trim();
            //PARSING THE BODY
            CharStream bodyStream = new ANTLRInputStream(pieceParts.get(1));
            BodyLexer bodyLexer = new BodyLexer(bodyStream);
            TokenStream bodyTokens = new CommonTokenStream(bodyLexer);
            bodyParser = new BodyParser(bodyTokens);      
            bodyTree = bodyParser.root();      
            bodyWalker = new ParseTreeWalker();
            bodyListener = new MakeBody(getKey(tempKey), headerListener.numerator(), headerListener.denominator());
            bodyWalker.walk(bodyListener, bodyTree);
            body = bodyListener.getBody();
        }catch (IOException | RuntimeException e){ 
            throw new RuntimeException(e);
        }           
        return new Song(header, body);
    }
    
    /**
     * Splits an input string into a header string and a body string.
     * The header must have the last line of its String be a Key definition.
     * @param input String from .ABC file
     * @return as list of strings where the first element is the header and the second is the body.
     */
    private static List<String> splitPiece(String input){
        int bodyPosition=0;
        String[] lines = input.split("" + '\n');
        List<String> lineList = Arrays.asList(lines);
        for (String curLine : lineList){ 
            if (curLine.startsWith("K:")){
                bodyPosition = lineList.indexOf(curLine) + 1;
            }
        }
        StringBuilder headerBuilder = new StringBuilder();
        StringBuilder bodyBuilder = new StringBuilder();
        for (int lineNum = 0; lineNum < lineList.size(); lineNum++){
            if (lineNum < bodyPosition ){
                headerBuilder.append(lineList.get(lineNum) + '\n');
            }
            else{
                bodyBuilder.append(lineList.get(lineNum) + '\n');
            }
        }
        return Arrays.asList(headerBuilder.toString(), bodyBuilder.toString().replaceAll(" ", ""));
    }

    /**
     * Slices list a la Python, start inclusive, end exclusive.
     * @param list
     * @param start the index to start slicing
     * @param end the index to slice up to
     * @return a slice of the original list, as described above.
     */
    private <E> List<E> slice(List<E> list, int start, int end) {
        List<E> slice = new ArrayList<>();
        for (int i=start; i<end; i++) {
            slice.add(list.get(i));
        }
        return slice;
    }
    
    /**
     * Get a map of all the sharps and flats associated with a given key of the form
     * "Cbm", where "C" is a capital letter A-G, "b" is a "b" or "#" representing a flat or 
     * sharp, and can be empty, and "m" which is present if the key is minor.
     * @param key a string as described above
     * @return a mapping from capital letters A-G representing the note to a 1 or -1 if the note
     *   is sharp or flat respectively. Notes without flats or sharps are omitted from the map.
     */
    public static Map<String, Integer> getKey(String key) {
        Map<String, Integer> C = new HashMap<>();
        Map<String, Integer> G = new HashMap<>();
        G.put("F", 1);
        Map<String, Integer> D = new HashMap<>(G);
        D.put("C", 1);
        Map<String, Integer> A = new HashMap<>(D);
        A.put("G", 1);
        Map<String, Integer> E = new HashMap<>(A);
        E.put("D", 1);
        Map<String, Integer> B = new HashMap<>(E);
        B.put("A", 1);
        Map<String, Integer> Fsharp = new HashMap<>(B);
        Fsharp.put("E", 1);
        Map<String, Integer> Csharp = new HashMap<>(Fsharp);
        Csharp.put("B", 1);
        
        Map<String, Integer> F = new HashMap<>();
        F.put("B", -1);
        Map<String, Integer> Bb = new HashMap<>(F);
        Bb.put("E", -1);
        Map<String, Integer> Eb = new HashMap<>(Bb);
        Eb.put("A", -1);
        Map<String, Integer> Ab = new HashMap<>(Eb);
        Ab.put("D", -1);
        Map<String, Integer> Db = new HashMap<>(Ab);
        Db.put("G", -1);
        Map<String, Integer> Gb = new HashMap<>(Db);
        Gb.put("C", -1);
        Map<String, Integer> Cb = new HashMap<>(Gb);
        Cb.put("F", -1);
        
        Map<String, Map<String, Integer>> keys = new HashMap<>();
        // major        ; relative minor
        keys.put("C", C); keys.put("Am", C);
        keys.put("G", G); keys.put("Em", G);
        keys.put("D", D); keys.put("Bm", D);
        keys.put("A", A); keys.put("F#m", A);
        keys.put("E", E); keys.put("C#m", E);
        keys.put("B", B); keys.put("G#m", B);
        keys.put("F#", Fsharp); keys.put("D#m", Fsharp);
        keys.put("C#", Csharp); keys.put("A#m", Csharp);
        
        keys.put("F", F); keys.put("Dm", F);
        keys.put("Bb", Bb); keys.put("Gm", Bb);
        keys.put("Eb", Eb); keys.put("Cm", Eb);
        keys.put("Ab", Ab); keys.put("Fm", Ab);
        keys.put("Db", Db); keys.put("Bbm", Db);
        keys.put("Gb", Gb); keys.put("Ebm", Gb);
        keys.put("Cb", Cb); keys.put("Abm", Cb);
        
        // for the sake of safety; I'll reuse some of applyMeasure and want to ensure it doesn't mutate
        return Collections.unmodifiableMap(keys.get(key));
    }
    
}
