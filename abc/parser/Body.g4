/*
 * Compile all your grammars using
 *       java -jar ../../../lib/antlr.jar *.g4
 * then Refresh in Eclipse.
 */

grammar Body;

import Configuration;

/* tell Antlr to ignore comments spaces around tokens. */
SPACES : [ ]+ -> skip;
COMMENT: '%' .*? NEWLINE -> skip; /* the *? means not greedy */

DIGIT: [0-9];
NEWLINE: ( ( '\n' ) | ('\r\n') );
VOICENAME: 'V:' ' '* ([a-z] | DIGIT)+;
START: '[|' | '|]' | '|:' | '||';
END: ':|' '[2'? | '|[' DIGIT;
barline: START | END | '|' | '[2';



root: NEWLINE* ( voice | (voicelabel NEWLINE+ voice NEWLINE+)+ ) EOF;
voice: barline? measure ( NEWLINE? barline NEWLINE? measure NEWLINE?)* barline? NEWLINE?;
measure: note+;

voicelabel: VOICENAME;

note: (single | chord | duplet | triplet | quadruplet | rest);
single: (('_'*) | ('^'*) | ('='*)) ('A'|'B'|'C'|'D'|'E'|'F'|'G'|'a'|'b'|'c'|'d'|'e'|'f'|'g') ((','*) | ('\''*)) ( DIGIT+ | (DIGIT* '/' DIGIT*) )?;
chord: '[' single+ ']';
duplet: '(2' (single|chord) (single|chord);
triplet: '(3' (single|chord) (single|chord) (single|chord);
quadruplet: '(4' (single|chord) (single|chord) (single|chord) (single|chord);
rest: 'z' (DIGIT | DIGIT* '/' DIGIT*)?;

