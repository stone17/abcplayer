/*
 * Compile all your grammars using
 *       java -jar ../../../lib/antlr.jar *.g4
 * then Refresh in Eclipse.
 */

grammar Header;

import Configuration;

SPACE: ' ';
DIGIT: [0-9];
fraction: DIGIT+ '/' DIGIT+;
LETTER: [a-zA-Z'.'','\-];
decimal: (DIGIT+ ('.' DIGIT+)?) | ('.' DIGIT+);
NEWLINE: SPACE* ( ( '\n' ) | ('\r\n') );

root: index title headerpart* key EOF;


index: 'X:' SPACE* DIGIT+ NEWLINE;
title: 'T:' (~NEWLINE)+ NEWLINE;

voice: 'V:' (~NEWLINE)+;
composer: 'C:' (~NEWLINE)+;
note_length: 'L:' SPACE*  (fraction | decimal);
tempo: 'Q:' SPACE*  fraction '=' decimal;
meter: 'M:' SPACE* (( LETTER '|'? )|fraction);
headerpart: (voice | composer | note_length | tempo | meter) NEWLINE;

key: 'K:' SPACE* LETTER ('#'|'b')? 'm'? NEWLINE?;
