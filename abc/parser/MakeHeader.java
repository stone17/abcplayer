package abc.parser;

import java.util.HashMap;
import java.util.Map;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import abc.parser.HeaderParser.VoiceContext;

public class MakeHeader implements HeaderListener {

    private Map<Character, String> header = new HashMap<Character, String>();
    private int num;
    private int den;
    //private final String meter;
    
    public Map<Character, String> getHeader(){ return new HashMap<Character,String>(header);}
    
    @Override public void enterEveryRule(ParserRuleContext arg0) {}
    @Override public void exitEveryRule(ParserRuleContext arg0) {}
    @Override public void visitErrorNode(ErrorNode arg0) {}
    @Override public void visitTerminal(TerminalNode arg0) {}
    @Override public void enterFraction(HeaderParser.FractionContext ctx) {}
    @Override public void exitFraction(HeaderParser.FractionContext ctx) {}
    @Override public void enterDecimal(HeaderParser.DecimalContext ctx) {}
    @Override public void exitDecimal(HeaderParser.DecimalContext ctx) {}
    @Override public void enterRoot(HeaderParser.RootContext ctx) {}
    @Override public void exitRoot(HeaderParser.RootContext ctx) {}
    @Override public void enterHeaderpart(HeaderParser.HeaderpartContext ctx) {}
    @Override public void exitHeaderpart(HeaderParser.HeaderpartContext ctx) {} 

    @Override public void enterIndex(HeaderParser.IndexContext ctx) {}
    @Override public void exitIndex(HeaderParser.IndexContext ctx) {
        String index = ctx.getText();
        header.put('I', index.replaceAll("[^0-9]", ""));
    }

    @Override public void enterTitle(HeaderParser.TitleContext ctx) {}
    @Override
    public void exitTitle(HeaderParser.TitleContext ctx) {
        String title = ctx.getText();
        header.put('T', title);
    }

    @Override public void enterKey(HeaderParser.KeyContext ctx) {}
    @Override
    public void exitKey(HeaderParser.KeyContext ctx) {
        String key = ctx.getText();
        header.put('K', key);
    }

    @Override public void enterComposer(HeaderParser.ComposerContext ctx) {}
    @Override
    public void exitComposer(HeaderParser.ComposerContext ctx) {
        String composer = ctx.getText();
        header.put('C', composer);
    }

    @Override public void enterNote_length(HeaderParser.Note_lengthContext ctx) {}
    @Override
    public void exitNote_length(HeaderParser.Note_lengthContext ctx) {
        String noteLength = ctx.getText();
        header.put('L', noteLength);
    }

    @Override public void enterTempo(HeaderParser.TempoContext ctx) {}
    @Override
    public void exitTempo(HeaderParser.TempoContext ctx) {
        String tempo = ctx.getText();
        header.put('Q', tempo);
    }

    @Override public void enterMeter(HeaderParser.MeterContext ctx) {}
    @Override
    public void exitMeter(HeaderParser.MeterContext ctx) {
        String meter = ctx.getText();
        String[] frac = meter.split(":");
        if (frac[1].equals("C")){
            num = 4;
            den = 4;
            meter = "M:4/4";
        }
        else if (frac[1].equals("C|")){
            num = 2;
            den = 2;
            meter = "M:2/2";
        }
        else{
            String[] numbs = frac[1].split("/");
            //this.meter = meter;
            num = Integer.valueOf(numbs[0]);
            den = Integer.valueOf(numbs[1]);
            header.put('M', meter);
        }
    }
    
    /**@return the meter*/
    /*public int getMeter(){
        return 
    }*/
    /**@return the numerator value of the song we passed.  Is 4 if no time signature*/
    public int numerator(){
        return num;
    }
    
    /**@return the denominator of song.  Is 4 if no time signature*/
    public int denominator(){
        return den;
    }

    @Override
    public void enterVoice(VoiceContext ctx) { }

    @Override
    public void exitVoice(VoiceContext ctx) { }

}
