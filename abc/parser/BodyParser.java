// Generated from src/abc/parser/Body.g4 by ANTLR 4.5.1

package abc.parser;
// Do not edit this .java file! Edit the .g4 file and re-run Antlr.

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BodyParser extends Parser {
  static { RuntimeMetaData.checkVersion("4.5.1", RuntimeMetaData.VERSION); }

  protected static final DFA[] _decisionToDFA;
  protected static final PredictionContextCache _sharedContextCache =
    new PredictionContextCache();
  public static final int
    T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
    T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, 
    T__16=17, T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, 
    T__23=24, T__24=25, T__25=26, T__26=27, T__27=28, SPACES=29, COMMENT=30, 
    DIGIT=31, NEWLINE=32, VOICENAME=33, START=34, END=35;
  public static final int
    RULE_barline = 0, RULE_root = 1, RULE_voice = 2, RULE_measure = 3, RULE_voicelabel = 4, 
    RULE_note = 5, RULE_single = 6, RULE_chord = 7, RULE_duplet = 8, RULE_triplet = 9, 
    RULE_quadruplet = 10, RULE_rest = 11;
  public static final String[] ruleNames = {
    "barline", "root", "voice", "measure", "voicelabel", "note", "single", 
    "chord", "duplet", "triplet", "quadruplet", "rest"
  };

  private static final String[] _LITERAL_NAMES = {
    null, "'|'", "'[2'", "'_'", "'^'", "'='", "'A'", "'B'", "'C'", "'D'", 
    "'E'", "'F'", "'G'", "'a'", "'b'", "'c'", "'d'", "'e'", "'f'", "'g'", 
    "','", "'''", "'/'", "'['", "']'", "'(2'", "'(3'", "'(4'", "'z'"
  };
  private static final String[] _SYMBOLIC_NAMES = {
    null, null, null, null, null, null, null, null, null, null, null, null, 
    null, null, null, null, null, null, null, null, null, null, null, null, 
    null, null, null, null, null, "SPACES", "COMMENT", "DIGIT", "NEWLINE", 
    "VOICENAME", "START", "END"
  };
  public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

  /**
   * @deprecated Use {@link #VOCABULARY} instead.
   */
  @Deprecated
  public static final String[] tokenNames;
  static {
    tokenNames = new String[_SYMBOLIC_NAMES.length];
    for (int i = 0; i < tokenNames.length; i++) {
      tokenNames[i] = VOCABULARY.getLiteralName(i);
      if (tokenNames[i] == null) {
        tokenNames[i] = VOCABULARY.getSymbolicName(i);
      }

      if (tokenNames[i] == null) {
        tokenNames[i] = "<INVALID>";
      }
    }
  }

  @Override
  @Deprecated
  public String[] getTokenNames() {
    return tokenNames;
  }

  @Override

  public Vocabulary getVocabulary() {
    return VOCABULARY;
  }

  @Override
  public String getGrammarFileName() { return "Body.g4"; }

  @Override
  public String[] getRuleNames() { return ruleNames; }

  @Override
  public String getSerializedATN() { return _serializedATN; }

  @Override
  public ATN getATN() { return _ATN; }


      // This method makes the parser stop running if it encounters
      // invalid input and throw a RuntimeException.
      public void reportErrorsAsExceptions() {
          // To prevent any reports to standard error, add this line:
          removeErrorListeners();
          
          addErrorListener(new BaseErrorListener() {
              public void syntaxError(Recognizer<?, ?> recognizer,
                                      Object offendingSymbol, 
                                      int line, int charPositionInLine,
                                      String msg, RecognitionException e) {
                  throw new ParseCancellationException(msg, e);
              }
          });
      }

  public BodyParser(TokenStream input) {
    super(input);
    _interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
  }
  public static class BarlineContext extends ParserRuleContext {
    public TerminalNode START() { return getToken(BodyParser.START, 0); }
    public TerminalNode END() { return getToken(BodyParser.END, 0); }
    public BarlineContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_barline; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof BodyListener ) ((BodyListener)listener).enterBarline(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof BodyListener ) ((BodyListener)listener).exitBarline(this);
    }
  }

  public final BarlineContext barline() throws RecognitionException {
    BarlineContext _localctx = new BarlineContext(_ctx, getState());
    enterRule(_localctx, 0, RULE_barline);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(24);
      _la = _input.LA(1);
      if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << START) | (1L << END))) != 0)) ) {
      _errHandler.recoverInline(this);
      } else {
        consume();
      }
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class RootContext extends ParserRuleContext {
    public TerminalNode EOF() { return getToken(BodyParser.EOF, 0); }
    public List<VoiceContext> voice() {
      return getRuleContexts(VoiceContext.class);
    }
    public VoiceContext voice(int i) {
      return getRuleContext(VoiceContext.class,i);
    }
    public List<TerminalNode> NEWLINE() { return getTokens(BodyParser.NEWLINE); }
    public TerminalNode NEWLINE(int i) {
      return getToken(BodyParser.NEWLINE, i);
    }
    public List<VoicelabelContext> voicelabel() {
      return getRuleContexts(VoicelabelContext.class);
    }
    public VoicelabelContext voicelabel(int i) {
      return getRuleContext(VoicelabelContext.class,i);
    }
    public RootContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_root; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof BodyListener ) ((BodyListener)listener).enterRoot(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof BodyListener ) ((BodyListener)listener).exitRoot(this);
    }
  }

  public final RootContext root() throws RecognitionException {
    RootContext _localctx = new RootContext(_ctx, getState());
    enterRule(_localctx, 2, RULE_root);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(29);
      _errHandler.sync(this);
      _la = _input.LA(1);
      while (_la==NEWLINE) {
        {
        {
        setState(26);
        match(NEWLINE);
        }
        }
        setState(31);
        _errHandler.sync(this);
        _la = _input.LA(1);
      }
      setState(49);
      switch (_input.LA(1)) {
      case T__0:
      case T__1:
      case T__2:
      case T__3:
      case T__4:
      case T__5:
      case T__6:
      case T__7:
      case T__8:
      case T__9:
      case T__10:
      case T__11:
      case T__12:
      case T__13:
      case T__14:
      case T__15:
      case T__16:
      case T__17:
      case T__18:
      case T__22:
      case T__24:
      case T__25:
      case T__26:
      case T__27:
      case START:
      case END:
        {
        setState(32);
        voice();
        }
        break;
      case VOICENAME:
        {
        setState(45); 
        _errHandler.sync(this);
        _la = _input.LA(1);
        do {
          {
          {
          setState(33);
          voicelabel();
          setState(35); 
          _errHandler.sync(this);
          _la = _input.LA(1);
          do {
            {
            {
            setState(34);
            match(NEWLINE);
            }
            }
            setState(37); 
            _errHandler.sync(this);
            _la = _input.LA(1);
          } while ( _la==NEWLINE );
          setState(39);
          voice();
          setState(41); 
          _errHandler.sync(this);
          _la = _input.LA(1);
          do {
            {
            {
            setState(40);
            match(NEWLINE);
            }
            }
            setState(43); 
            _errHandler.sync(this);
            _la = _input.LA(1);
          } while ( _la==NEWLINE );
          }
          }
          setState(47); 
          _errHandler.sync(this);
          _la = _input.LA(1);
        } while ( _la==VOICENAME );
        }
        break;
      default:
        throw new NoViableAltException(this);
      }
      setState(51);
      match(EOF);
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class VoiceContext extends ParserRuleContext {
    public List<MeasureContext> measure() {
      return getRuleContexts(MeasureContext.class);
    }
    public MeasureContext measure(int i) {
      return getRuleContext(MeasureContext.class,i);
    }
    public List<BarlineContext> barline() {
      return getRuleContexts(BarlineContext.class);
    }
    public BarlineContext barline(int i) {
      return getRuleContext(BarlineContext.class,i);
    }
    public List<TerminalNode> NEWLINE() { return getTokens(BodyParser.NEWLINE); }
    public TerminalNode NEWLINE(int i) {
      return getToken(BodyParser.NEWLINE, i);
    }
    public VoiceContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_voice; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof BodyListener ) ((BodyListener)listener).enterVoice(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof BodyListener ) ((BodyListener)listener).exitVoice(this);
    }
  }

  public final VoiceContext voice() throws RecognitionException {
    VoiceContext _localctx = new VoiceContext(_ctx, getState());
    enterRule(_localctx, 4, RULE_voice);
    int _la;
    try {
      int _alt;
      enterOuterAlt(_localctx, 1);
      {
      setState(54);
      _la = _input.LA(1);
      if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << START) | (1L << END))) != 0)) {
        {
        setState(53);
        barline();
        }
      }

      setState(56);
      measure();
      setState(70);
      _errHandler.sync(this);
      _alt = getInterpreter().adaptivePredict(_input,9,_ctx);
      while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
        if ( _alt==1 ) {
          {
          {
          setState(58);
          _la = _input.LA(1);
          if (_la==NEWLINE) {
            {
            setState(57);
            match(NEWLINE);
            }
          }

          setState(60);
          barline();
          setState(62);
          _la = _input.LA(1);
          if (_la==NEWLINE) {
            {
            setState(61);
            match(NEWLINE);
            }
          }

          setState(64);
          measure();
          setState(66);
          switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
          case 1:
            {
            setState(65);
            match(NEWLINE);
            }
            break;
          }
          }
          } 
        }
        setState(72);
        _errHandler.sync(this);
        _alt = getInterpreter().adaptivePredict(_input,9,_ctx);
      }
      setState(74);
      _la = _input.LA(1);
      if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << START) | (1L << END))) != 0)) {
        {
        setState(73);
        barline();
        }
      }

      setState(77);
      switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
      case 1:
        {
        setState(76);
        match(NEWLINE);
        }
        break;
      }
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class MeasureContext extends ParserRuleContext {
    public List<NoteContext> note() {
      return getRuleContexts(NoteContext.class);
    }
    public NoteContext note(int i) {
      return getRuleContext(NoteContext.class,i);
    }
    public MeasureContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_measure; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof BodyListener ) ((BodyListener)listener).enterMeasure(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof BodyListener ) ((BodyListener)listener).exitMeasure(this);
    }
  }

  public final MeasureContext measure() throws RecognitionException {
    MeasureContext _localctx = new MeasureContext(_ctx, getState());
    enterRule(_localctx, 6, RULE_measure);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(80); 
      _errHandler.sync(this);
      _la = _input.LA(1);
      do {
        {
        {
        setState(79);
        note();
        }
        }
        setState(82); 
        _errHandler.sync(this);
        _la = _input.LA(1);
      } while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__3) | (1L << T__4) | (1L << T__5) | (1L << T__6) | (1L << T__7) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__22) | (1L << T__24) | (1L << T__25) | (1L << T__26) | (1L << T__27))) != 0) );
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class VoicelabelContext extends ParserRuleContext {
    public TerminalNode VOICENAME() { return getToken(BodyParser.VOICENAME, 0); }
    public VoicelabelContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_voicelabel; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof BodyListener ) ((BodyListener)listener).enterVoicelabel(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof BodyListener ) ((BodyListener)listener).exitVoicelabel(this);
    }
  }

  public final VoicelabelContext voicelabel() throws RecognitionException {
    VoicelabelContext _localctx = new VoicelabelContext(_ctx, getState());
    enterRule(_localctx, 8, RULE_voicelabel);
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(84);
      match(VOICENAME);
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class NoteContext extends ParserRuleContext {
    public SingleContext single() {
      return getRuleContext(SingleContext.class,0);
    }
    public ChordContext chord() {
      return getRuleContext(ChordContext.class,0);
    }
    public DupletContext duplet() {
      return getRuleContext(DupletContext.class,0);
    }
    public TripletContext triplet() {
      return getRuleContext(TripletContext.class,0);
    }
    public QuadrupletContext quadruplet() {
      return getRuleContext(QuadrupletContext.class,0);
    }
    public RestContext rest() {
      return getRuleContext(RestContext.class,0);
    }
    public NoteContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_note; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof BodyListener ) ((BodyListener)listener).enterNote(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof BodyListener ) ((BodyListener)listener).exitNote(this);
    }
  }

  public final NoteContext note() throws RecognitionException {
    NoteContext _localctx = new NoteContext(_ctx, getState());
    enterRule(_localctx, 10, RULE_note);
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(92);
      switch (_input.LA(1)) {
      case T__2:
      case T__3:
      case T__4:
      case T__5:
      case T__6:
      case T__7:
      case T__8:
      case T__9:
      case T__10:
      case T__11:
      case T__12:
      case T__13:
      case T__14:
      case T__15:
      case T__16:
      case T__17:
      case T__18:
        {
        setState(86);
        single();
        }
        break;
      case T__22:
        {
        setState(87);
        chord();
        }
        break;
      case T__24:
        {
        setState(88);
        duplet();
        }
        break;
      case T__25:
        {
        setState(89);
        triplet();
        }
        break;
      case T__26:
        {
        setState(90);
        quadruplet();
        }
        break;
      case T__27:
        {
        setState(91);
        rest();
        }
        break;
      default:
        throw new NoViableAltException(this);
      }
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class SingleContext extends ParserRuleContext {
    public List<TerminalNode> DIGIT() { return getTokens(BodyParser.DIGIT); }
    public TerminalNode DIGIT(int i) {
      return getToken(BodyParser.DIGIT, i);
    }
    public SingleContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_single; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof BodyListener ) ((BodyListener)listener).enterSingle(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof BodyListener ) ((BodyListener)listener).exitSingle(this);
    }
  }

  public final SingleContext single() throws RecognitionException {
    SingleContext _localctx = new SingleContext(_ctx, getState());
    enterRule(_localctx, 12, RULE_single);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(112);
      switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
      case 1:
        {
        {
        setState(97);
        _errHandler.sync(this);
        _la = _input.LA(1);
        while (_la==T__2) {
          {
          {
          setState(94);
          match(T__2);
          }
          }
          setState(99);
          _errHandler.sync(this);
          _la = _input.LA(1);
        }
        }
        }
        break;
      case 2:
        {
        {
        setState(103);
        _errHandler.sync(this);
        _la = _input.LA(1);
        while (_la==T__3) {
          {
          {
          setState(100);
          match(T__3);
          }
          }
          setState(105);
          _errHandler.sync(this);
          _la = _input.LA(1);
        }
        }
        }
        break;
      case 3:
        {
        {
        setState(109);
        _errHandler.sync(this);
        _la = _input.LA(1);
        while (_la==T__4) {
          {
          {
          setState(106);
          match(T__4);
          }
          }
          setState(111);
          _errHandler.sync(this);
          _la = _input.LA(1);
        }
        }
        }
        break;
      }
      setState(114);
      _la = _input.LA(1);
      if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__5) | (1L << T__6) | (1L << T__7) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18))) != 0)) ) {
      _errHandler.recoverInline(this);
      } else {
        consume();
      }
      setState(127);
      switch ( getInterpreter().adaptivePredict(_input,20,_ctx) ) {
      case 1:
        {
        {
        setState(118);
        _errHandler.sync(this);
        _la = _input.LA(1);
        while (_la==T__19) {
          {
          {
          setState(115);
          match(T__19);
          }
          }
          setState(120);
          _errHandler.sync(this);
          _la = _input.LA(1);
        }
        }
        }
        break;
      case 2:
        {
        {
        setState(124);
        _errHandler.sync(this);
        _la = _input.LA(1);
        while (_la==T__20) {
          {
          {
          setState(121);
          match(T__20);
          }
          }
          setState(126);
          _errHandler.sync(this);
          _la = _input.LA(1);
        }
        }
        }
        break;
      }
      setState(147);
      switch ( getInterpreter().adaptivePredict(_input,24,_ctx) ) {
      case 1:
        {
        setState(130); 
        _errHandler.sync(this);
        _la = _input.LA(1);
        do {
          {
          {
          setState(129);
          match(DIGIT);
          }
          }
          setState(132); 
          _errHandler.sync(this);
          _la = _input.LA(1);
        } while ( _la==DIGIT );
        }
        break;
      case 2:
        {
        {
        setState(137);
        _errHandler.sync(this);
        _la = _input.LA(1);
        while (_la==DIGIT) {
          {
          {
          setState(134);
          match(DIGIT);
          }
          }
          setState(139);
          _errHandler.sync(this);
          _la = _input.LA(1);
        }
        setState(140);
        match(T__21);
        setState(144);
        _errHandler.sync(this);
        _la = _input.LA(1);
        while (_la==DIGIT) {
          {
          {
          setState(141);
          match(DIGIT);
          }
          }
          setState(146);
          _errHandler.sync(this);
          _la = _input.LA(1);
        }
        }
        }
        break;
      }
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class ChordContext extends ParserRuleContext {
    public List<SingleContext> single() {
      return getRuleContexts(SingleContext.class);
    }
    public SingleContext single(int i) {
      return getRuleContext(SingleContext.class,i);
    }
    public ChordContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_chord; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof BodyListener ) ((BodyListener)listener).enterChord(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof BodyListener ) ((BodyListener)listener).exitChord(this);
    }
  }

  public final ChordContext chord() throws RecognitionException {
    ChordContext _localctx = new ChordContext(_ctx, getState());
    enterRule(_localctx, 14, RULE_chord);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(149);
      match(T__22);
      setState(151); 
      _errHandler.sync(this);
      _la = _input.LA(1);
      do {
        {
        {
        setState(150);
        single();
        }
        }
        setState(153); 
        _errHandler.sync(this);
        _la = _input.LA(1);
      } while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__3) | (1L << T__4) | (1L << T__5) | (1L << T__6) | (1L << T__7) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18))) != 0) );
      setState(155);
      match(T__23);
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class DupletContext extends ParserRuleContext {
    public List<SingleContext> single() {
      return getRuleContexts(SingleContext.class);
    }
    public SingleContext single(int i) {
      return getRuleContext(SingleContext.class,i);
    }
    public List<ChordContext> chord() {
      return getRuleContexts(ChordContext.class);
    }
    public ChordContext chord(int i) {
      return getRuleContext(ChordContext.class,i);
    }
    public DupletContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_duplet; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof BodyListener ) ((BodyListener)listener).enterDuplet(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof BodyListener ) ((BodyListener)listener).exitDuplet(this);
    }
  }

  public final DupletContext duplet() throws RecognitionException {
    DupletContext _localctx = new DupletContext(_ctx, getState());
    enterRule(_localctx, 16, RULE_duplet);
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(157);
      match(T__24);
      setState(160);
      switch (_input.LA(1)) {
      case T__2:
      case T__3:
      case T__4:
      case T__5:
      case T__6:
      case T__7:
      case T__8:
      case T__9:
      case T__10:
      case T__11:
      case T__12:
      case T__13:
      case T__14:
      case T__15:
      case T__16:
      case T__17:
      case T__18:
        {
        setState(158);
        single();
        }
        break;
      case T__22:
        {
        setState(159);
        chord();
        }
        break;
      default:
        throw new NoViableAltException(this);
      }
      setState(164);
      switch (_input.LA(1)) {
      case T__2:
      case T__3:
      case T__4:
      case T__5:
      case T__6:
      case T__7:
      case T__8:
      case T__9:
      case T__10:
      case T__11:
      case T__12:
      case T__13:
      case T__14:
      case T__15:
      case T__16:
      case T__17:
      case T__18:
        {
        setState(162);
        single();
        }
        break;
      case T__22:
        {
        setState(163);
        chord();
        }
        break;
      default:
        throw new NoViableAltException(this);
      }
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class TripletContext extends ParserRuleContext {
    public List<SingleContext> single() {
      return getRuleContexts(SingleContext.class);
    }
    public SingleContext single(int i) {
      return getRuleContext(SingleContext.class,i);
    }
    public List<ChordContext> chord() {
      return getRuleContexts(ChordContext.class);
    }
    public ChordContext chord(int i) {
      return getRuleContext(ChordContext.class,i);
    }
    public TripletContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_triplet; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof BodyListener ) ((BodyListener)listener).enterTriplet(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof BodyListener ) ((BodyListener)listener).exitTriplet(this);
    }
  }

  public final TripletContext triplet() throws RecognitionException {
    TripletContext _localctx = new TripletContext(_ctx, getState());
    enterRule(_localctx, 18, RULE_triplet);
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(166);
      match(T__25);
      setState(169);
      switch (_input.LA(1)) {
      case T__2:
      case T__3:
      case T__4:
      case T__5:
      case T__6:
      case T__7:
      case T__8:
      case T__9:
      case T__10:
      case T__11:
      case T__12:
      case T__13:
      case T__14:
      case T__15:
      case T__16:
      case T__17:
      case T__18:
        {
        setState(167);
        single();
        }
        break;
      case T__22:
        {
        setState(168);
        chord();
        }
        break;
      default:
        throw new NoViableAltException(this);
      }
      setState(173);
      switch (_input.LA(1)) {
      case T__2:
      case T__3:
      case T__4:
      case T__5:
      case T__6:
      case T__7:
      case T__8:
      case T__9:
      case T__10:
      case T__11:
      case T__12:
      case T__13:
      case T__14:
      case T__15:
      case T__16:
      case T__17:
      case T__18:
        {
        setState(171);
        single();
        }
        break;
      case T__22:
        {
        setState(172);
        chord();
        }
        break;
      default:
        throw new NoViableAltException(this);
      }
      setState(177);
      switch (_input.LA(1)) {
      case T__2:
      case T__3:
      case T__4:
      case T__5:
      case T__6:
      case T__7:
      case T__8:
      case T__9:
      case T__10:
      case T__11:
      case T__12:
      case T__13:
      case T__14:
      case T__15:
      case T__16:
      case T__17:
      case T__18:
        {
        setState(175);
        single();
        }
        break;
      case T__22:
        {
        setState(176);
        chord();
        }
        break;
      default:
        throw new NoViableAltException(this);
      }
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class QuadrupletContext extends ParserRuleContext {
    public List<SingleContext> single() {
      return getRuleContexts(SingleContext.class);
    }
    public SingleContext single(int i) {
      return getRuleContext(SingleContext.class,i);
    }
    public List<ChordContext> chord() {
      return getRuleContexts(ChordContext.class);
    }
    public ChordContext chord(int i) {
      return getRuleContext(ChordContext.class,i);
    }
    public QuadrupletContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_quadruplet; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof BodyListener ) ((BodyListener)listener).enterQuadruplet(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof BodyListener ) ((BodyListener)listener).exitQuadruplet(this);
    }
  }

  public final QuadrupletContext quadruplet() throws RecognitionException {
    QuadrupletContext _localctx = new QuadrupletContext(_ctx, getState());
    enterRule(_localctx, 20, RULE_quadruplet);
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(179);
      match(T__26);
      setState(182);
      switch (_input.LA(1)) {
      case T__2:
      case T__3:
      case T__4:
      case T__5:
      case T__6:
      case T__7:
      case T__8:
      case T__9:
      case T__10:
      case T__11:
      case T__12:
      case T__13:
      case T__14:
      case T__15:
      case T__16:
      case T__17:
      case T__18:
        {
        setState(180);
        single();
        }
        break;
      case T__22:
        {
        setState(181);
        chord();
        }
        break;
      default:
        throw new NoViableAltException(this);
      }
      setState(186);
      switch (_input.LA(1)) {
      case T__2:
      case T__3:
      case T__4:
      case T__5:
      case T__6:
      case T__7:
      case T__8:
      case T__9:
      case T__10:
      case T__11:
      case T__12:
      case T__13:
      case T__14:
      case T__15:
      case T__16:
      case T__17:
      case T__18:
        {
        setState(184);
        single();
        }
        break;
      case T__22:
        {
        setState(185);
        chord();
        }
        break;
      default:
        throw new NoViableAltException(this);
      }
      setState(190);
      switch (_input.LA(1)) {
      case T__2:
      case T__3:
      case T__4:
      case T__5:
      case T__6:
      case T__7:
      case T__8:
      case T__9:
      case T__10:
      case T__11:
      case T__12:
      case T__13:
      case T__14:
      case T__15:
      case T__16:
      case T__17:
      case T__18:
        {
        setState(188);
        single();
        }
        break;
      case T__22:
        {
        setState(189);
        chord();
        }
        break;
      default:
        throw new NoViableAltException(this);
      }
      setState(194);
      switch (_input.LA(1)) {
      case T__2:
      case T__3:
      case T__4:
      case T__5:
      case T__6:
      case T__7:
      case T__8:
      case T__9:
      case T__10:
      case T__11:
      case T__12:
      case T__13:
      case T__14:
      case T__15:
      case T__16:
      case T__17:
      case T__18:
        {
        setState(192);
        single();
        }
        break;
      case T__22:
        {
        setState(193);
        chord();
        }
        break;
      default:
        throw new NoViableAltException(this);
      }
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class RestContext extends ParserRuleContext {
    public List<TerminalNode> DIGIT() { return getTokens(BodyParser.DIGIT); }
    public TerminalNode DIGIT(int i) {
      return getToken(BodyParser.DIGIT, i);
    }
    public RestContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_rest; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof BodyListener ) ((BodyListener)listener).enterRest(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof BodyListener ) ((BodyListener)listener).exitRest(this);
    }
  }

  public final RestContext rest() throws RecognitionException {
    RestContext _localctx = new RestContext(_ctx, getState());
    enterRule(_localctx, 22, RULE_rest);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(196);
      match(T__27);
      setState(211);
      switch ( getInterpreter().adaptivePredict(_input,37,_ctx) ) {
      case 1:
        {
        setState(197);
        match(DIGIT);
        }
        break;
      case 2:
        {
        setState(201);
        _errHandler.sync(this);
        _la = _input.LA(1);
        while (_la==DIGIT) {
          {
          {
          setState(198);
          match(DIGIT);
          }
          }
          setState(203);
          _errHandler.sync(this);
          _la = _input.LA(1);
        }
        setState(204);
        match(T__21);
        setState(208);
        _errHandler.sync(this);
        _la = _input.LA(1);
        while (_la==DIGIT) {
          {
          {
          setState(205);
          match(DIGIT);
          }
          }
          setState(210);
          _errHandler.sync(this);
          _la = _input.LA(1);
        }
        }
        break;
      }
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static final String _serializedATN =
    "\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3%\u00d8\4\2\t\2"+
      "\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
      "\13\t\13\4\f\t\f\4\r\t\r\3\2\3\2\3\3\7\3\36\n\3\f\3\16\3!\13\3\3\3"+
      "\3\3\3\3\6\3&\n\3\r\3\16\3\'\3\3\3\3\6\3,\n\3\r\3\16\3-\6\3\60\n\3"+
      "\r\3\16\3\61\5\3\64\n\3\3\3\3\3\3\4\5\49\n\4\3\4\3\4\5\4=\n\4\3\4"+
      "\3\4\5\4A\n\4\3\4\3\4\5\4E\n\4\7\4G\n\4\f\4\16\4J\13\4\3\4\5\4M\n"+
      "\4\3\4\5\4P\n\4\3\5\6\5S\n\5\r\5\16\5T\3\6\3\6\3\7\3\7\3\7\3\7\3\7"+
      "\3\7\5\7_\n\7\3\b\7\bb\n\b\f\b\16\be\13\b\3\b\7\bh\n\b\f\b\16\bk\13"+
      "\b\3\b\7\bn\n\b\f\b\16\bq\13\b\5\bs\n\b\3\b\3\b\7\bw\n\b\f\b\16\b"+
      "z\13\b\3\b\7\b}\n\b\f\b\16\b\u0080\13\b\5\b\u0082\n\b\3\b\6\b\u0085"+
      "\n\b\r\b\16\b\u0086\3\b\7\b\u008a\n\b\f\b\16\b\u008d\13\b\3\b\3\b"+
      "\7\b\u0091\n\b\f\b\16\b\u0094\13\b\5\b\u0096\n\b\3\t\3\t\6\t\u009a"+
      "\n\t\r\t\16\t\u009b\3\t\3\t\3\n\3\n\3\n\5\n\u00a3\n\n\3\n\3\n\5\n"+
      "\u00a7\n\n\3\13\3\13\3\13\5\13\u00ac\n\13\3\13\3\13\5\13\u00b0\n\13"+
      "\3\13\3\13\5\13\u00b4\n\13\3\f\3\f\3\f\5\f\u00b9\n\f\3\f\3\f\5\f\u00bd"+
      "\n\f\3\f\3\f\5\f\u00c1\n\f\3\f\3\f\5\f\u00c5\n\f\3\r\3\r\3\r\7\r\u00ca"+
      "\n\r\f\r\16\r\u00cd\13\r\3\r\3\r\7\r\u00d1\n\r\f\r\16\r\u00d4\13\r"+
      "\5\r\u00d6\n\r\3\r\2\2\16\2\4\6\b\n\f\16\20\22\24\26\30\2\4\4\2\3"+
      "\4$%\3\2\b\25\u00f8\2\32\3\2\2\2\4\37\3\2\2\2\68\3\2\2\2\bR\3\2\2"+
      "\2\nV\3\2\2\2\f^\3\2\2\2\16r\3\2\2\2\20\u0097\3\2\2\2\22\u009f\3\2"+
      "\2\2\24\u00a8\3\2\2\2\26\u00b5\3\2\2\2\30\u00c6\3\2\2\2\32\33\t\2"+
      "\2\2\33\3\3\2\2\2\34\36\7\"\2\2\35\34\3\2\2\2\36!\3\2\2\2\37\35\3"+
      "\2\2\2\37 \3\2\2\2 \63\3\2\2\2!\37\3\2\2\2\"\64\5\6\4\2#%\5\n\6\2"+
      "$&\7\"\2\2%$\3\2\2\2&\'\3\2\2\2\'%\3\2\2\2\'(\3\2\2\2()\3\2\2\2)+"+
      "\5\6\4\2*,\7\"\2\2+*\3\2\2\2,-\3\2\2\2-+\3\2\2\2-.\3\2\2\2.\60\3\2"+
      "\2\2/#\3\2\2\2\60\61\3\2\2\2\61/\3\2\2\2\61\62\3\2\2\2\62\64\3\2\2"+
      "\2\63\"\3\2\2\2\63/\3\2\2\2\64\65\3\2\2\2\65\66\7\2\2\3\66\5\3\2\2"+
      "\2\679\5\2\2\28\67\3\2\2\289\3\2\2\29:\3\2\2\2:H\5\b\5\2;=\7\"\2\2"+
      "<;\3\2\2\2<=\3\2\2\2=>\3\2\2\2>@\5\2\2\2?A\7\"\2\2@?\3\2\2\2@A\3\2"+
      "\2\2AB\3\2\2\2BD\5\b\5\2CE\7\"\2\2DC\3\2\2\2DE\3\2\2\2EG\3\2\2\2F"+
      "<\3\2\2\2GJ\3\2\2\2HF\3\2\2\2HI\3\2\2\2IL\3\2\2\2JH\3\2\2\2KM\5\2"+
      "\2\2LK\3\2\2\2LM\3\2\2\2MO\3\2\2\2NP\7\"\2\2ON\3\2\2\2OP\3\2\2\2P"+
      "\7\3\2\2\2QS\5\f\7\2RQ\3\2\2\2ST\3\2\2\2TR\3\2\2\2TU\3\2\2\2U\t\3"+
      "\2\2\2VW\7#\2\2W\13\3\2\2\2X_\5\16\b\2Y_\5\20\t\2Z_\5\22\n\2[_\5\24"+
      "\13\2\\_\5\26\f\2]_\5\30\r\2^X\3\2\2\2^Y\3\2\2\2^Z\3\2\2\2^[\3\2\2"+
      "\2^\\\3\2\2\2^]\3\2\2\2_\r\3\2\2\2`b\7\5\2\2a`\3\2\2\2be\3\2\2\2c"+
      "a\3\2\2\2cd\3\2\2\2ds\3\2\2\2ec\3\2\2\2fh\7\6\2\2gf\3\2\2\2hk\3\2"+
      "\2\2ig\3\2\2\2ij\3\2\2\2js\3\2\2\2ki\3\2\2\2ln\7\7\2\2ml\3\2\2\2n"+
      "q\3\2\2\2om\3\2\2\2op\3\2\2\2ps\3\2\2\2qo\3\2\2\2rc\3\2\2\2ri\3\2"+
      "\2\2ro\3\2\2\2st\3\2\2\2t\u0081\t\3\2\2uw\7\26\2\2vu\3\2\2\2wz\3\2"+
      "\2\2xv\3\2\2\2xy\3\2\2\2y\u0082\3\2\2\2zx\3\2\2\2{}\7\27\2\2|{\3\2"+
      "\2\2}\u0080\3\2\2\2~|\3\2\2\2~\177\3\2\2\2\177\u0082\3\2\2\2\u0080"+
      "~\3\2\2\2\u0081x\3\2\2\2\u0081~\3\2\2\2\u0082\u0095\3\2\2\2\u0083"+
      "\u0085\7!\2\2\u0084\u0083\3\2\2\2\u0085\u0086\3\2\2\2\u0086\u0084"+
      "\3\2\2\2\u0086\u0087\3\2\2\2\u0087\u0096\3\2\2\2\u0088\u008a\7!\2"+
      "\2\u0089\u0088\3\2\2\2\u008a\u008d\3\2\2\2\u008b\u0089\3\2\2\2\u008b"+
      "\u008c\3\2\2\2\u008c\u008e\3\2\2\2\u008d\u008b\3\2\2\2\u008e\u0092"+
      "\7\30\2\2\u008f\u0091\7!\2\2\u0090\u008f\3\2\2\2\u0091\u0094\3\2\2"+
      "\2\u0092\u0090\3\2\2\2\u0092\u0093\3\2\2\2\u0093\u0096\3\2\2\2\u0094"+
      "\u0092\3\2\2\2\u0095\u0084\3\2\2\2\u0095\u008b\3\2\2\2\u0095\u0096"+
      "\3\2\2\2\u0096\17\3\2\2\2\u0097\u0099\7\31\2\2\u0098\u009a\5\16\b"+
      "\2\u0099\u0098\3\2\2\2\u009a\u009b\3\2\2\2\u009b\u0099\3\2\2\2\u009b"+
      "\u009c\3\2\2\2\u009c\u009d\3\2\2\2\u009d\u009e\7\32\2\2\u009e\21\3"+
      "\2\2\2\u009f\u00a2\7\33\2\2\u00a0\u00a3\5\16\b\2\u00a1\u00a3\5\20"+
      "\t\2\u00a2\u00a0\3\2\2\2\u00a2\u00a1\3\2\2\2\u00a3\u00a6\3\2\2\2\u00a4"+
      "\u00a7\5\16\b\2\u00a5\u00a7\5\20\t\2\u00a6\u00a4\3\2\2\2\u00a6\u00a5"+
      "\3\2\2\2\u00a7\23\3\2\2\2\u00a8\u00ab\7\34\2\2\u00a9\u00ac\5\16\b"+
      "\2\u00aa\u00ac\5\20\t\2\u00ab\u00a9\3\2\2\2\u00ab\u00aa\3\2\2\2\u00ac"+
      "\u00af\3\2\2\2\u00ad\u00b0\5\16\b\2\u00ae\u00b0\5\20\t\2\u00af\u00ad"+
      "\3\2\2\2\u00af\u00ae\3\2\2\2\u00b0\u00b3\3\2\2\2\u00b1\u00b4\5\16"+
      "\b\2\u00b2\u00b4\5\20\t\2\u00b3\u00b1\3\2\2\2\u00b3\u00b2\3\2\2\2"+
      "\u00b4\25\3\2\2\2\u00b5\u00b8\7\35\2\2\u00b6\u00b9\5\16\b\2\u00b7"+
      "\u00b9\5\20\t\2\u00b8\u00b6\3\2\2\2\u00b8\u00b7\3\2\2\2\u00b9\u00bc"+
      "\3\2\2\2\u00ba\u00bd\5\16\b\2\u00bb\u00bd\5\20\t\2\u00bc\u00ba\3\2"+
      "\2\2\u00bc\u00bb\3\2\2\2\u00bd\u00c0\3\2\2\2\u00be\u00c1\5\16\b\2"+
      "\u00bf\u00c1\5\20\t\2\u00c0\u00be\3\2\2\2\u00c0\u00bf\3\2\2\2\u00c1"+
      "\u00c4\3\2\2\2\u00c2\u00c5\5\16\b\2\u00c3\u00c5\5\20\t\2\u00c4\u00c2"+
      "\3\2\2\2\u00c4\u00c3\3\2\2\2\u00c5\27\3\2\2\2\u00c6\u00d5\7\36\2\2"+
      "\u00c7\u00d6\7!\2\2\u00c8\u00ca\7!\2\2\u00c9\u00c8\3\2\2\2\u00ca\u00cd"+
      "\3\2\2\2\u00cb\u00c9\3\2\2\2\u00cb\u00cc\3\2\2\2\u00cc\u00ce\3\2\2"+
      "\2\u00cd\u00cb\3\2\2\2\u00ce\u00d2\7\30\2\2\u00cf\u00d1\7!\2\2\u00d0"+
      "\u00cf\3\2\2\2\u00d1\u00d4\3\2\2\2\u00d2\u00d0\3\2\2\2\u00d2\u00d3"+
      "\3\2\2\2\u00d3\u00d6\3\2\2\2\u00d4\u00d2\3\2\2\2\u00d5\u00c7\3\2\2"+
      "\2\u00d5\u00cb\3\2\2\2\u00d5\u00d6\3\2\2\2\u00d6\31\3\2\2\2(\37\'"+
      "-\61\638<@DHLOT^ciorx~\u0081\u0086\u008b\u0092\u0095\u009b\u00a2\u00a6"+
      "\u00ab\u00af\u00b3\u00b8\u00bc\u00c0\u00c4\u00cb\u00d2\u00d5";
  public static final ATN _ATN =
    new ATNDeserializer().deserialize(_serializedATN.toCharArray());
  static {
    _decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
    for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
      _decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
    }
  }
}