// Generated from src/abc/parser/Body.g4 by ANTLR 4.5.1

package abc.parser;
// Do not edit this .java file! Edit the .g4 file and re-run Antlr.

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link BodyParser}.
 */
public interface BodyListener extends ParseTreeListener {
  /**
   * Enter a parse tree produced by {@link BodyParser#barline}.
   * @param ctx the parse tree
   */
  void enterBarline(BodyParser.BarlineContext ctx);
  /**
   * Exit a parse tree produced by {@link BodyParser#barline}.
   * @param ctx the parse tree
   */
  void exitBarline(BodyParser.BarlineContext ctx);
  /**
   * Enter a parse tree produced by {@link BodyParser#root}.
   * @param ctx the parse tree
   */
  void enterRoot(BodyParser.RootContext ctx);
  /**
   * Exit a parse tree produced by {@link BodyParser#root}.
   * @param ctx the parse tree
   */
  void exitRoot(BodyParser.RootContext ctx);
  /**
   * Enter a parse tree produced by {@link BodyParser#voice}.
   * @param ctx the parse tree
   */
  void enterVoice(BodyParser.VoiceContext ctx);
  /**
   * Exit a parse tree produced by {@link BodyParser#voice}.
   * @param ctx the parse tree
   */
  void exitVoice(BodyParser.VoiceContext ctx);
  /**
   * Enter a parse tree produced by {@link BodyParser#measure}.
   * @param ctx the parse tree
   */
  void enterMeasure(BodyParser.MeasureContext ctx);
  /**
   * Exit a parse tree produced by {@link BodyParser#measure}.
   * @param ctx the parse tree
   */
  void exitMeasure(BodyParser.MeasureContext ctx);
  /**
   * Enter a parse tree produced by {@link BodyParser#voicelabel}.
   * @param ctx the parse tree
   */
  void enterVoicelabel(BodyParser.VoicelabelContext ctx);
  /**
   * Exit a parse tree produced by {@link BodyParser#voicelabel}.
   * @param ctx the parse tree
   */
  void exitVoicelabel(BodyParser.VoicelabelContext ctx);
  /**
   * Enter a parse tree produced by {@link BodyParser#note}.
   * @param ctx the parse tree
   */
  void enterNote(BodyParser.NoteContext ctx);
  /**
   * Exit a parse tree produced by {@link BodyParser#note}.
   * @param ctx the parse tree
   */
  void exitNote(BodyParser.NoteContext ctx);
  /**
   * Enter a parse tree produced by {@link BodyParser#single}.
   * @param ctx the parse tree
   */
  void enterSingle(BodyParser.SingleContext ctx);
  /**
   * Exit a parse tree produced by {@link BodyParser#single}.
   * @param ctx the parse tree
   */
  void exitSingle(BodyParser.SingleContext ctx);
  /**
   * Enter a parse tree produced by {@link BodyParser#chord}.
   * @param ctx the parse tree
   */
  void enterChord(BodyParser.ChordContext ctx);
  /**
   * Exit a parse tree produced by {@link BodyParser#chord}.
   * @param ctx the parse tree
   */
  void exitChord(BodyParser.ChordContext ctx);
  /**
   * Enter a parse tree produced by {@link BodyParser#duplet}.
   * @param ctx the parse tree
   */
  void enterDuplet(BodyParser.DupletContext ctx);
  /**
   * Exit a parse tree produced by {@link BodyParser#duplet}.
   * @param ctx the parse tree
   */
  void exitDuplet(BodyParser.DupletContext ctx);
  /**
   * Enter a parse tree produced by {@link BodyParser#triplet}.
   * @param ctx the parse tree
   */
  void enterTriplet(BodyParser.TripletContext ctx);
  /**
   * Exit a parse tree produced by {@link BodyParser#triplet}.
   * @param ctx the parse tree
   */
  void exitTriplet(BodyParser.TripletContext ctx);
  /**
   * Enter a parse tree produced by {@link BodyParser#quadruplet}.
   * @param ctx the parse tree
   */
  void enterQuadruplet(BodyParser.QuadrupletContext ctx);
  /**
   * Exit a parse tree produced by {@link BodyParser#quadruplet}.
   * @param ctx the parse tree
   */
  void exitQuadruplet(BodyParser.QuadrupletContext ctx);
  /**
   * Enter a parse tree produced by {@link BodyParser#rest}.
   * @param ctx the parse tree
   */
  void enterRest(BodyParser.RestContext ctx);
  /**
   * Exit a parse tree produced by {@link BodyParser#rest}.
   * @param ctx the parse tree
   */
  void exitRest(BodyParser.RestContext ctx);
}