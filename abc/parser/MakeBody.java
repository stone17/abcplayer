package abc.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import abc.parser.BodyParser.BarlineContext;
import abc.parser.BodyParser.RestContext;
import abc.parser.BodyParser.VoiceContext;
import abc.parser.BodyParser.VoicelabelContext;
import abc.sound.Chord;
import abc.sound.Duplet;
import abc.sound.Measure;
import abc.sound.Note;
import abc.sound.Quadruplet;
import abc.sound.Rest;
import abc.sound.Single;
import abc.sound.Structure;
import abc.sound.Triplet;

public class MakeBody implements BodyListener {
    
    private Map<String, List<Structure>> voices = new HashMap<String, List<Structure>>();
    private List<Structure> measures = new ArrayList<Structure>();
    private List<Note> notes = new ArrayList<Note>();
    
    private Map<String, Integer> key;
    
    private String currentVoiceName = "none";
    private Stack<Note> stack = new Stack<Note>();
    private final int num;
    private final int den;
    
    public MakeBody(Map<String, Integer> inputKey, int num, int den){
        this.num = num;
        this.den = den;
        this.key = new HashMap<String, Integer>(inputKey);
    }
    
    public Map<String, List<Structure>> getBody(){ return new HashMap<String, List<Structure>>(voices);}
    
    @Override public void enterEveryRule(ParserRuleContext arg0) {}
    @Override public void exitEveryRule(ParserRuleContext arg0) {}
    @Override public void visitErrorNode(ErrorNode arg0) {}
    @Override public void visitTerminal(TerminalNode arg0) {}
    @Override public void enterRoot(BodyParser.RootContext ctx) {}
    @Override public void exitRoot(BodyParser.RootContext ctx) {}
    
    @Override
    public void enterMeasure(BodyParser.MeasureContext ctx) {
        notes = new ArrayList<Note>();
    }

    @Override
    public void exitMeasure(BodyParser.MeasureContext ctx) {
        measures.add(new Measure(notes, key, num, den));
    }    

    @Override public void enterNote(BodyParser.NoteContext ctx) {}
    @Override
    public void exitNote(BodyParser.NoteContext ctx) {
        notes.add(stack.pop());
    }

    @Override public void enterSingle(BodyParser.SingleContext ctx) {}
    @Override
    public void exitSingle(BodyParser.SingleContext ctx) {
        String single = ctx.getText();
        stack.push(new Single(single));
    }

    @Override public void enterChord(BodyParser.ChordContext ctx) {}
    @Override
    public void exitChord(BodyParser.ChordContext ctx) {
        List<BodyParser.SingleContext> things = ctx.single();
        List<Note> chordNotes = new ArrayList<Note>();
        for(int i = 0; i < things.size(); i++){
            chordNotes.add(stack.pop());
        }
        stack.push(new Chord(chordNotes));       
    }

    @Override
    public void enterDuplet(BodyParser.DupletContext ctx) {}
    @Override
    public void exitDuplet(BodyParser.DupletContext ctx) {
        Note note2 = stack.pop();
        Note note1= stack.pop();
        stack.push(new Duplet(note1, note2));
    }

    @Override public void enterTriplet(BodyParser.TripletContext ctx) {}
    @Override
    public void exitTriplet(BodyParser.TripletContext ctx) {
        Note note3 = stack.pop();
        Note note2 = stack.pop();
        Note note1 = stack.pop();
        stack.push(new Triplet(note1,note2,note3));
    }

    @Override public void enterQuadruplet(BodyParser.QuadrupletContext ctx) {}
    @Override
    public void exitQuadruplet(BodyParser.QuadrupletContext ctx) {
        Note note4 = stack.pop();
        Note note3 = stack.pop();
        Note note2 = stack.pop();
        Note note1 = stack.pop();
        stack.push(new Quadruplet(note1, note2, note3, note4));
    }

    @Override

    public void enterBarline(BarlineContext ctx) { }

    @Override
    public void exitBarline(BarlineContext ctx) {
        if (ctx.START() != null) {
            measures.add(Structure.makeBeginRepeat());
        } else if (ctx.END() != null) {
            String text = ctx.END().getText();
            if (text.contains("1")) {
                measures.add(Structure.makeFirstEnding());
            } else {
                measures.add(Structure.makeEndRepeat());
            }
        }
    }

    public void enterRest(RestContext ctx) { }

    @Override
    public void exitRest(RestContext ctx) {
        String rest = ctx.getText();
        stack.push(new Rest(rest));
        
    }
    
    @Override
    public void enterVoice(VoiceContext ctx) {
        measures = new ArrayList<Structure>();
    }

    @Override
    public void exitVoice(VoiceContext ctx) {
        if (voices.containsKey(currentVoiceName)){
            List<Structure> currentMeasures = voices.get(currentVoiceName);
            for(Structure thing : measures){
                currentMeasures.add(thing);
            }
        }
        else voices.put(currentVoiceName, measures);
    }

    @Override public void enterVoicelabel(VoicelabelContext ctx) {}

    @Override
    public void exitVoicelabel(VoicelabelContext ctx) {
        currentVoiceName = ctx.getText();       
    }

}
