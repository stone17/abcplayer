// Generated from src/abc/parser/Header.g4 by ANTLR 4.5.1

package abc.parser;
// Do not edit this .java file! Edit the .g4 file and re-run Antlr.

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class HeaderParser extends Parser {
  static { RuntimeMetaData.checkVersion("4.5.1", RuntimeMetaData.VERSION); }

  protected static final DFA[] _decisionToDFA;
  protected static final PredictionContextCache _sharedContextCache =
    new PredictionContextCache();
  public static final int
    T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
    T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, SPACE=16, 
    DIGIT=17, LETTER=18, NEWLINE=19;
  public static final int
    RULE_fraction = 0, RULE_decimal = 1, RULE_root = 2, RULE_index = 3, 
    RULE_title = 4, RULE_voice = 5, RULE_composer = 6, RULE_note_length = 7, 
    RULE_tempo = 8, RULE_meter = 9, RULE_headerpart = 10, RULE_key = 11;
  public static final String[] ruleNames = {
    "fraction", "decimal", "root", "index", "title", "voice", "composer", 
    "note_length", "tempo", "meter", "headerpart", "key"
  };

  private static final String[] _LITERAL_NAMES = {
    null, "'/'", "'.'", "'X:'", "'T:'", "'V:'", "'C:'", "'L:'", "'Q:'", 
    "'='", "'M:'", "'|'", "'K:'", "'#'", "'b'", "'m'", "' '"
  };
  private static final String[] _SYMBOLIC_NAMES = {
    null, null, null, null, null, null, null, null, null, null, null, null, 
    null, null, null, null, "SPACE", "DIGIT", "LETTER", "NEWLINE"
  };
  public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

  /**
   * @deprecated Use {@link #VOCABULARY} instead.
   */
  @Deprecated
  public static final String[] tokenNames;
  static {
    tokenNames = new String[_SYMBOLIC_NAMES.length];
    for (int i = 0; i < tokenNames.length; i++) {
      tokenNames[i] = VOCABULARY.getLiteralName(i);
      if (tokenNames[i] == null) {
        tokenNames[i] = VOCABULARY.getSymbolicName(i);
      }

      if (tokenNames[i] == null) {
        tokenNames[i] = "<INVALID>";
      }
    }
  }

  @Override
  @Deprecated
  public String[] getTokenNames() {
    return tokenNames;
  }

  @Override

  public Vocabulary getVocabulary() {
    return VOCABULARY;
  }

  @Override
  public String getGrammarFileName() { return "Header.g4"; }

  @Override
  public String[] getRuleNames() { return ruleNames; }

  @Override
  public String getSerializedATN() { return _serializedATN; }

  @Override
  public ATN getATN() { return _ATN; }


      // This method makes the parser stop running if it encounters
      // invalid input and throw a RuntimeException.
      public void reportErrorsAsExceptions() {
          // To prevent any reports to standard error, add this line:
          removeErrorListeners();
          
          addErrorListener(new BaseErrorListener() {
              public void syntaxError(Recognizer<?, ?> recognizer,
                                      Object offendingSymbol, 
                                      int line, int charPositionInLine,
                                      String msg, RecognitionException e) {
                  throw new ParseCancellationException(msg, e);
              }
          });
      }

  public HeaderParser(TokenStream input) {
    super(input);
    _interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
  }
  public static class FractionContext extends ParserRuleContext {
    public List<TerminalNode> DIGIT() { return getTokens(HeaderParser.DIGIT); }
    public TerminalNode DIGIT(int i) {
      return getToken(HeaderParser.DIGIT, i);
    }
    public FractionContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_fraction; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof HeaderListener ) ((HeaderListener)listener).enterFraction(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof HeaderListener ) ((HeaderListener)listener).exitFraction(this);
    }
  }

  public final FractionContext fraction() throws RecognitionException {
    FractionContext _localctx = new FractionContext(_ctx, getState());
    enterRule(_localctx, 0, RULE_fraction);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(25); 
      _errHandler.sync(this);
      _la = _input.LA(1);
      do {
        {
        {
        setState(24);
        match(DIGIT);
        }
        }
        setState(27); 
        _errHandler.sync(this);
        _la = _input.LA(1);
      } while ( _la==DIGIT );
      setState(29);
      match(T__0);
      setState(31); 
      _errHandler.sync(this);
      _la = _input.LA(1);
      do {
        {
        {
        setState(30);
        match(DIGIT);
        }
        }
        setState(33); 
        _errHandler.sync(this);
        _la = _input.LA(1);
      } while ( _la==DIGIT );
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class DecimalContext extends ParserRuleContext {
    public List<TerminalNode> DIGIT() { return getTokens(HeaderParser.DIGIT); }
    public TerminalNode DIGIT(int i) {
      return getToken(HeaderParser.DIGIT, i);
    }
    public DecimalContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_decimal; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof HeaderListener ) ((HeaderListener)listener).enterDecimal(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof HeaderListener ) ((HeaderListener)listener).exitDecimal(this);
    }
  }

  public final DecimalContext decimal() throws RecognitionException {
    DecimalContext _localctx = new DecimalContext(_ctx, getState());
    enterRule(_localctx, 2, RULE_decimal);
    int _la;
    try {
      setState(54);
      switch (_input.LA(1)) {
      case DIGIT:
        enterOuterAlt(_localctx, 1);
        {
        {
        setState(36); 
        _errHandler.sync(this);
        _la = _input.LA(1);
        do {
          {
          {
          setState(35);
          match(DIGIT);
          }
          }
          setState(38); 
          _errHandler.sync(this);
          _la = _input.LA(1);
        } while ( _la==DIGIT );
        setState(46);
        _la = _input.LA(1);
        if (_la==T__1) {
          {
          setState(40);
          match(T__1);
          setState(42); 
          _errHandler.sync(this);
          _la = _input.LA(1);
          do {
            {
            {
            setState(41);
            match(DIGIT);
            }
            }
            setState(44); 
            _errHandler.sync(this);
            _la = _input.LA(1);
          } while ( _la==DIGIT );
          }
        }

        }
        }
        break;
      case T__1:
        enterOuterAlt(_localctx, 2);
        {
        {
        setState(48);
        match(T__1);
        setState(50); 
        _errHandler.sync(this);
        _la = _input.LA(1);
        do {
          {
          {
          setState(49);
          match(DIGIT);
          }
          }
          setState(52); 
          _errHandler.sync(this);
          _la = _input.LA(1);
        } while ( _la==DIGIT );
        }
        }
        break;
      default:
        throw new NoViableAltException(this);
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class RootContext extends ParserRuleContext {
    public IndexContext index() {
      return getRuleContext(IndexContext.class,0);
    }
    public TitleContext title() {
      return getRuleContext(TitleContext.class,0);
    }
    public KeyContext key() {
      return getRuleContext(KeyContext.class,0);
    }
    public TerminalNode EOF() { return getToken(HeaderParser.EOF, 0); }
    public List<HeaderpartContext> headerpart() {
      return getRuleContexts(HeaderpartContext.class);
    }
    public HeaderpartContext headerpart(int i) {
      return getRuleContext(HeaderpartContext.class,i);
    }
    public RootContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_root; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof HeaderListener ) ((HeaderListener)listener).enterRoot(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof HeaderListener ) ((HeaderListener)listener).exitRoot(this);
    }
  }

  public final RootContext root() throws RecognitionException {
    RootContext _localctx = new RootContext(_ctx, getState());
    enterRule(_localctx, 4, RULE_root);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(56);
      index();
      setState(57);
      title();
      setState(61);
      _errHandler.sync(this);
      _la = _input.LA(1);
      while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__4) | (1L << T__5) | (1L << T__6) | (1L << T__7) | (1L << T__9))) != 0)) {
        {
        {
        setState(58);
        headerpart();
        }
        }
        setState(63);
        _errHandler.sync(this);
        _la = _input.LA(1);
      }
      setState(64);
      key();
      setState(65);
      match(EOF);
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class IndexContext extends ParserRuleContext {
    public TerminalNode NEWLINE() { return getToken(HeaderParser.NEWLINE, 0); }
    public List<TerminalNode> SPACE() { return getTokens(HeaderParser.SPACE); }
    public TerminalNode SPACE(int i) {
      return getToken(HeaderParser.SPACE, i);
    }
    public List<TerminalNode> DIGIT() { return getTokens(HeaderParser.DIGIT); }
    public TerminalNode DIGIT(int i) {
      return getToken(HeaderParser.DIGIT, i);
    }
    public IndexContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_index; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof HeaderListener ) ((HeaderListener)listener).enterIndex(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof HeaderListener ) ((HeaderListener)listener).exitIndex(this);
    }
  }

  public final IndexContext index() throws RecognitionException {
    IndexContext _localctx = new IndexContext(_ctx, getState());
    enterRule(_localctx, 6, RULE_index);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(67);
      match(T__2);
      setState(71);
      _errHandler.sync(this);
      _la = _input.LA(1);
      while (_la==SPACE) {
        {
        {
        setState(68);
        match(SPACE);
        }
        }
        setState(73);
        _errHandler.sync(this);
        _la = _input.LA(1);
      }
      setState(75); 
      _errHandler.sync(this);
      _la = _input.LA(1);
      do {
        {
        {
        setState(74);
        match(DIGIT);
        }
        }
        setState(77); 
        _errHandler.sync(this);
        _la = _input.LA(1);
      } while ( _la==DIGIT );
      setState(79);
      match(NEWLINE);
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class TitleContext extends ParserRuleContext {
    public List<TerminalNode> NEWLINE() { return getTokens(HeaderParser.NEWLINE); }
    public TerminalNode NEWLINE(int i) {
      return getToken(HeaderParser.NEWLINE, i);
    }
    public TitleContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_title; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof HeaderListener ) ((HeaderListener)listener).enterTitle(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof HeaderListener ) ((HeaderListener)listener).exitTitle(this);
    }
  }

  public final TitleContext title() throws RecognitionException {
    TitleContext _localctx = new TitleContext(_ctx, getState());
    enterRule(_localctx, 8, RULE_title);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(81);
      match(T__3);
      setState(83); 
      _errHandler.sync(this);
      _la = _input.LA(1);
      do {
        {
        {
        setState(82);
        _la = _input.LA(1);
        if ( _la <= 0 || (_la==NEWLINE) ) {
        _errHandler.recoverInline(this);
        } else {
          consume();
        }
        }
        }
        setState(85); 
        _errHandler.sync(this);
        _la = _input.LA(1);
      } while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << T__2) | (1L << T__3) | (1L << T__4) | (1L << T__5) | (1L << T__6) | (1L << T__7) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << SPACE) | (1L << DIGIT) | (1L << LETTER))) != 0) );
      setState(87);
      match(NEWLINE);
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class VoiceContext extends ParserRuleContext {
    public List<TerminalNode> NEWLINE() { return getTokens(HeaderParser.NEWLINE); }
    public TerminalNode NEWLINE(int i) {
      return getToken(HeaderParser.NEWLINE, i);
    }
    public VoiceContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_voice; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof HeaderListener ) ((HeaderListener)listener).enterVoice(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof HeaderListener ) ((HeaderListener)listener).exitVoice(this);
    }
  }

  public final VoiceContext voice() throws RecognitionException {
    VoiceContext _localctx = new VoiceContext(_ctx, getState());
    enterRule(_localctx, 10, RULE_voice);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(89);
      match(T__4);
      setState(91); 
      _errHandler.sync(this);
      _la = _input.LA(1);
      do {
        {
        {
        setState(90);
        _la = _input.LA(1);
        if ( _la <= 0 || (_la==NEWLINE) ) {
        _errHandler.recoverInline(this);
        } else {
          consume();
        }
        }
        }
        setState(93); 
        _errHandler.sync(this);
        _la = _input.LA(1);
      } while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << T__2) | (1L << T__3) | (1L << T__4) | (1L << T__5) | (1L << T__6) | (1L << T__7) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << SPACE) | (1L << DIGIT) | (1L << LETTER))) != 0) );
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class ComposerContext extends ParserRuleContext {
    public List<TerminalNode> NEWLINE() { return getTokens(HeaderParser.NEWLINE); }
    public TerminalNode NEWLINE(int i) {
      return getToken(HeaderParser.NEWLINE, i);
    }
    public ComposerContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_composer; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof HeaderListener ) ((HeaderListener)listener).enterComposer(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof HeaderListener ) ((HeaderListener)listener).exitComposer(this);
    }
  }

  public final ComposerContext composer() throws RecognitionException {
    ComposerContext _localctx = new ComposerContext(_ctx, getState());
    enterRule(_localctx, 12, RULE_composer);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(95);
      match(T__5);
      setState(97); 
      _errHandler.sync(this);
      _la = _input.LA(1);
      do {
        {
        {
        setState(96);
        _la = _input.LA(1);
        if ( _la <= 0 || (_la==NEWLINE) ) {
        _errHandler.recoverInline(this);
        } else {
          consume();
        }
        }
        }
        setState(99); 
        _errHandler.sync(this);
        _la = _input.LA(1);
      } while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << T__2) | (1L << T__3) | (1L << T__4) | (1L << T__5) | (1L << T__6) | (1L << T__7) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << SPACE) | (1L << DIGIT) | (1L << LETTER))) != 0) );
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class Note_lengthContext extends ParserRuleContext {
    public FractionContext fraction() {
      return getRuleContext(FractionContext.class,0);
    }
    public DecimalContext decimal() {
      return getRuleContext(DecimalContext.class,0);
    }
    public List<TerminalNode> SPACE() { return getTokens(HeaderParser.SPACE); }
    public TerminalNode SPACE(int i) {
      return getToken(HeaderParser.SPACE, i);
    }
    public Note_lengthContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_note_length; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof HeaderListener ) ((HeaderListener)listener).enterNote_length(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof HeaderListener ) ((HeaderListener)listener).exitNote_length(this);
    }
  }

  public final Note_lengthContext note_length() throws RecognitionException {
    Note_lengthContext _localctx = new Note_lengthContext(_ctx, getState());
    enterRule(_localctx, 14, RULE_note_length);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(101);
      match(T__6);
      setState(105);
      _errHandler.sync(this);
      _la = _input.LA(1);
      while (_la==SPACE) {
        {
        {
        setState(102);
        match(SPACE);
        }
        }
        setState(107);
        _errHandler.sync(this);
        _la = _input.LA(1);
      }
      setState(110);
      switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
      case 1:
        {
        setState(108);
        fraction();
        }
        break;
      case 2:
        {
        setState(109);
        decimal();
        }
        break;
      }
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class TempoContext extends ParserRuleContext {
    public FractionContext fraction() {
      return getRuleContext(FractionContext.class,0);
    }
    public DecimalContext decimal() {
      return getRuleContext(DecimalContext.class,0);
    }
    public List<TerminalNode> SPACE() { return getTokens(HeaderParser.SPACE); }
    public TerminalNode SPACE(int i) {
      return getToken(HeaderParser.SPACE, i);
    }
    public TempoContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_tempo; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof HeaderListener ) ((HeaderListener)listener).enterTempo(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof HeaderListener ) ((HeaderListener)listener).exitTempo(this);
    }
  }

  public final TempoContext tempo() throws RecognitionException {
    TempoContext _localctx = new TempoContext(_ctx, getState());
    enterRule(_localctx, 16, RULE_tempo);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(112);
      match(T__7);
      setState(116);
      _errHandler.sync(this);
      _la = _input.LA(1);
      while (_la==SPACE) {
        {
        {
        setState(113);
        match(SPACE);
        }
        }
        setState(118);
        _errHandler.sync(this);
        _la = _input.LA(1);
      }
      setState(119);
      fraction();
      setState(120);
      match(T__8);
      setState(121);
      decimal();
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class MeterContext extends ParserRuleContext {
    public FractionContext fraction() {
      return getRuleContext(FractionContext.class,0);
    }
    public List<TerminalNode> SPACE() { return getTokens(HeaderParser.SPACE); }
    public TerminalNode SPACE(int i) {
      return getToken(HeaderParser.SPACE, i);
    }
    public TerminalNode LETTER() { return getToken(HeaderParser.LETTER, 0); }
    public MeterContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_meter; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof HeaderListener ) ((HeaderListener)listener).enterMeter(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof HeaderListener ) ((HeaderListener)listener).exitMeter(this);
    }
  }

  public final MeterContext meter() throws RecognitionException {
    MeterContext _localctx = new MeterContext(_ctx, getState());
    enterRule(_localctx, 18, RULE_meter);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(123);
      match(T__9);
      setState(127);
      _errHandler.sync(this);
      _la = _input.LA(1);
      while (_la==SPACE) {
        {
        {
        setState(124);
        match(SPACE);
        }
        }
        setState(129);
        _errHandler.sync(this);
        _la = _input.LA(1);
      }
      setState(135);
      switch (_input.LA(1)) {
      case LETTER:
        {
        {
        setState(130);
        match(LETTER);
        setState(132);
        _la = _input.LA(1);
        if (_la==T__10) {
          {
          setState(131);
          match(T__10);
          }
        }

        }
        }
        break;
      case DIGIT:
        {
        setState(134);
        fraction();
        }
        break;
      default:
        throw new NoViableAltException(this);
      }
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class HeaderpartContext extends ParserRuleContext {
    public TerminalNode NEWLINE() { return getToken(HeaderParser.NEWLINE, 0); }
    public VoiceContext voice() {
      return getRuleContext(VoiceContext.class,0);
    }
    public ComposerContext composer() {
      return getRuleContext(ComposerContext.class,0);
    }
    public Note_lengthContext note_length() {
      return getRuleContext(Note_lengthContext.class,0);
    }
    public TempoContext tempo() {
      return getRuleContext(TempoContext.class,0);
    }
    public MeterContext meter() {
      return getRuleContext(MeterContext.class,0);
    }
    public HeaderpartContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_headerpart; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof HeaderListener ) ((HeaderListener)listener).enterHeaderpart(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof HeaderListener ) ((HeaderListener)listener).exitHeaderpart(this);
    }
  }

  public final HeaderpartContext headerpart() throws RecognitionException {
    HeaderpartContext _localctx = new HeaderpartContext(_ctx, getState());
    enterRule(_localctx, 20, RULE_headerpart);
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(142);
      switch (_input.LA(1)) {
      case T__4:
        {
        setState(137);
        voice();
        }
        break;
      case T__5:
        {
        setState(138);
        composer();
        }
        break;
      case T__6:
        {
        setState(139);
        note_length();
        }
        break;
      case T__7:
        {
        setState(140);
        tempo();
        }
        break;
      case T__9:
        {
        setState(141);
        meter();
        }
        break;
      default:
        throw new NoViableAltException(this);
      }
      setState(144);
      match(NEWLINE);
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class KeyContext extends ParserRuleContext {
    public TerminalNode LETTER() { return getToken(HeaderParser.LETTER, 0); }
    public List<TerminalNode> SPACE() { return getTokens(HeaderParser.SPACE); }
    public TerminalNode SPACE(int i) {
      return getToken(HeaderParser.SPACE, i);
    }
    public TerminalNode NEWLINE() { return getToken(HeaderParser.NEWLINE, 0); }
    public KeyContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_key; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof HeaderListener ) ((HeaderListener)listener).enterKey(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof HeaderListener ) ((HeaderListener)listener).exitKey(this);
    }
  }

  public final KeyContext key() throws RecognitionException {
    KeyContext _localctx = new KeyContext(_ctx, getState());
    enterRule(_localctx, 22, RULE_key);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(146);
      match(T__11);
      setState(150);
      _errHandler.sync(this);
      _la = _input.LA(1);
      while (_la==SPACE) {
        {
        {
        setState(147);
        match(SPACE);
        }
        }
        setState(152);
        _errHandler.sync(this);
        _la = _input.LA(1);
      }
      setState(153);
      match(LETTER);
      setState(155);
      _la = _input.LA(1);
      if (_la==T__12 || _la==T__13) {
        {
        setState(154);
        _la = _input.LA(1);
        if ( !(_la==T__12 || _la==T__13) ) {
        _errHandler.recoverInline(this);
        } else {
          consume();
        }
        }
      }

      setState(158);
      _la = _input.LA(1);
      if (_la==T__14) {
        {
        setState(157);
        match(T__14);
        }
      }

      setState(161);
      _la = _input.LA(1);
      if (_la==NEWLINE) {
        {
        setState(160);
        match(NEWLINE);
        }
      }

      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static final String _serializedATN =
    "\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\25\u00a6\4\2\t"+
      "\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n"+
      "\4\13\t\13\4\f\t\f\4\r\t\r\3\2\6\2\34\n\2\r\2\16\2\35\3\2\3\2\6\2"+
      "\"\n\2\r\2\16\2#\3\3\6\3\'\n\3\r\3\16\3(\3\3\3\3\6\3-\n\3\r\3\16\3"+
      ".\5\3\61\n\3\3\3\3\3\6\3\65\n\3\r\3\16\3\66\5\39\n\3\3\4\3\4\3\4\7"+
      "\4>\n\4\f\4\16\4A\13\4\3\4\3\4\3\4\3\5\3\5\7\5H\n\5\f\5\16\5K\13\5"+
      "\3\5\6\5N\n\5\r\5\16\5O\3\5\3\5\3\6\3\6\6\6V\n\6\r\6\16\6W\3\6\3\6"+
      "\3\7\3\7\6\7^\n\7\r\7\16\7_\3\b\3\b\6\bd\n\b\r\b\16\be\3\t\3\t\7\t"+
      "j\n\t\f\t\16\tm\13\t\3\t\3\t\5\tq\n\t\3\n\3\n\7\nu\n\n\f\n\16\nx\13"+
      "\n\3\n\3\n\3\n\3\n\3\13\3\13\7\13\u0080\n\13\f\13\16\13\u0083\13\13"+
      "\3\13\3\13\5\13\u0087\n\13\3\13\5\13\u008a\n\13\3\f\3\f\3\f\3\f\3"+
      "\f\5\f\u0091\n\f\3\f\3\f\3\r\3\r\7\r\u0097\n\r\f\r\16\r\u009a\13\r"+
      "\3\r\3\r\5\r\u009e\n\r\3\r\5\r\u00a1\n\r\3\r\5\r\u00a4\n\r\3\r\2\2"+
      "\16\2\4\6\b\n\f\16\20\22\24\26\30\2\4\3\2\25\25\3\2\17\20\u00b4\2"+
      "\33\3\2\2\2\48\3\2\2\2\6:\3\2\2\2\bE\3\2\2\2\nS\3\2\2\2\f[\3\2\2\2"+
      "\16a\3\2\2\2\20g\3\2\2\2\22r\3\2\2\2\24}\3\2\2\2\26\u0090\3\2\2\2"+
      "\30\u0094\3\2\2\2\32\34\7\23\2\2\33\32\3\2\2\2\34\35\3\2\2\2\35\33"+
      "\3\2\2\2\35\36\3\2\2\2\36\37\3\2\2\2\37!\7\3\2\2 \"\7\23\2\2! \3\2"+
      "\2\2\"#\3\2\2\2#!\3\2\2\2#$\3\2\2\2$\3\3\2\2\2%\'\7\23\2\2&%\3\2\2"+
      "\2\'(\3\2\2\2(&\3\2\2\2()\3\2\2\2)\60\3\2\2\2*,\7\4\2\2+-\7\23\2\2"+
      ",+\3\2\2\2-.\3\2\2\2.,\3\2\2\2./\3\2\2\2/\61\3\2\2\2\60*\3\2\2\2\60"+
      "\61\3\2\2\2\619\3\2\2\2\62\64\7\4\2\2\63\65\7\23\2\2\64\63\3\2\2\2"+
      "\65\66\3\2\2\2\66\64\3\2\2\2\66\67\3\2\2\2\679\3\2\2\28&\3\2\2\28"+
      "\62\3\2\2\29\5\3\2\2\2:;\5\b\5\2;?\5\n\6\2<>\5\26\f\2=<\3\2\2\2>A"+
      "\3\2\2\2?=\3\2\2\2?@\3\2\2\2@B\3\2\2\2A?\3\2\2\2BC\5\30\r\2CD\7\2"+
      "\2\3D\7\3\2\2\2EI\7\5\2\2FH\7\22\2\2GF\3\2\2\2HK\3\2\2\2IG\3\2\2\2"+
      "IJ\3\2\2\2JM\3\2\2\2KI\3\2\2\2LN\7\23\2\2ML\3\2\2\2NO\3\2\2\2OM\3"+
      "\2\2\2OP\3\2\2\2PQ\3\2\2\2QR\7\25\2\2R\t\3\2\2\2SU\7\6\2\2TV\n\2\2"+
      "\2UT\3\2\2\2VW\3\2\2\2WU\3\2\2\2WX\3\2\2\2XY\3\2\2\2YZ\7\25\2\2Z\13"+
      "\3\2\2\2[]\7\7\2\2\\^\n\2\2\2]\\\3\2\2\2^_\3\2\2\2_]\3\2\2\2_`\3\2"+
      "\2\2`\r\3\2\2\2ac\7\b\2\2bd\n\2\2\2cb\3\2\2\2de\3\2\2\2ec\3\2\2\2"+
      "ef\3\2\2\2f\17\3\2\2\2gk\7\t\2\2hj\7\22\2\2ih\3\2\2\2jm\3\2\2\2ki"+
      "\3\2\2\2kl\3\2\2\2lp\3\2\2\2mk\3\2\2\2nq\5\2\2\2oq\5\4\3\2pn\3\2\2"+
      "\2po\3\2\2\2q\21\3\2\2\2rv\7\n\2\2su\7\22\2\2ts\3\2\2\2ux\3\2\2\2"+
      "vt\3\2\2\2vw\3\2\2\2wy\3\2\2\2xv\3\2\2\2yz\5\2\2\2z{\7\13\2\2{|\5"+
      "\4\3\2|\23\3\2\2\2}\u0081\7\f\2\2~\u0080\7\22\2\2\177~\3\2\2\2\u0080"+
      "\u0083\3\2\2\2\u0081\177\3\2\2\2\u0081\u0082\3\2\2\2\u0082\u0089\3"+
      "\2\2\2\u0083\u0081\3\2\2\2\u0084\u0086\7\24\2\2\u0085\u0087\7\r\2"+
      "\2\u0086\u0085\3\2\2\2\u0086\u0087\3\2\2\2\u0087\u008a\3\2\2\2\u0088"+
      "\u008a\5\2\2\2\u0089\u0084\3\2\2\2\u0089\u0088\3\2\2\2\u008a\25\3"+
      "\2\2\2\u008b\u0091\5\f\7\2\u008c\u0091\5\16\b\2\u008d\u0091\5\20\t"+
      "\2\u008e\u0091\5\22\n\2\u008f\u0091\5\24\13\2\u0090\u008b\3\2\2\2"+
      "\u0090\u008c\3\2\2\2\u0090\u008d\3\2\2\2\u0090\u008e\3\2\2\2\u0090"+
      "\u008f\3\2\2\2\u0091\u0092\3\2\2\2\u0092\u0093\7\25\2\2\u0093\27\3"+
      "\2\2\2\u0094\u0098\7\16\2\2\u0095\u0097\7\22\2\2\u0096\u0095\3\2\2"+
      "\2\u0097\u009a\3\2\2\2\u0098\u0096\3\2\2\2\u0098\u0099\3\2\2\2\u0099"+
      "\u009b\3\2\2\2\u009a\u0098\3\2\2\2\u009b\u009d\7\24\2\2\u009c\u009e"+
      "\t\3\2\2\u009d\u009c\3\2\2\2\u009d\u009e\3\2\2\2\u009e\u00a0\3\2\2"+
      "\2\u009f\u00a1\7\21\2\2\u00a0\u009f\3\2\2\2\u00a0\u00a1\3\2\2\2\u00a1"+
      "\u00a3\3\2\2\2\u00a2\u00a4\7\25\2\2\u00a3\u00a2\3\2\2\2\u00a3\u00a4"+
      "\3\2\2\2\u00a4\31\3\2\2\2\32\35#(.\60\668?IOW_ekpv\u0081\u0086\u0089"+
      "\u0090\u0098\u009d\u00a0\u00a3";
  public static final ATN _ATN =
    new ATNDeserializer().deserialize(_serializedATN.toCharArray());
  static {
    _decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
    for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
      _decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
    }
  }
}