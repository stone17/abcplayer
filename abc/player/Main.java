package abc.player;

import java.io.File;

import abc.sound.Song;

/**
 * Run from the command line with the location of the abc file to be played.
 */
public class Main {

    /**
     * Plays the input file using Java MIDI API and displays
     * header information to the standard output stream.
     * 
     * (Your code should not exit the application abnormally using
     * System.exit().)
     * 
     * @param file the name of input abc file
     */
    public static void play(String file) {
        Song.parseAndPlay(new File(file));
    }

    public static void main(String[] args) {
        play(args[0]);
    }
}
